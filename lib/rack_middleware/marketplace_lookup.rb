# Appends current marketplace info to env.
#
# Note: Is safe to run, even if there's no current_marketplace
class MarketplaceLookup
  def initialize(app)
    @app = app
  end

  def call(env)
    request = Rack::Request.new(env)
    host = MarketPlace::Utility::Url.strip_port_from_host(env['HTTP_HOST'])
    current_store = MarketPlace::Utility::Store.resolve_from_host(host)
    if current_store.present?
      current_plan = MarketPlace::Store.find_or_create_plan(current_store.try(:id)) 
    end
    @app.call(env.merge!(
      current_store: current_store,
      current_plan: current_plan))
  end
end
