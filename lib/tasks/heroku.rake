require 'bundler'

module Config
  module Integrations
    class Heroku2 < Struct.new(:app)
      def invoke
        puts 'Setting vars...'
        heroku_command = "config:set #{vars}"
        heroku(heroku_command)
        puts 'Vars set:'
        puts heroku_command
      end

      def vars
        # Load only local options to Heroku
        config_file = Config.load_files("./config/settings/#{environment}.yml")
        hash_to_vars = proc do |h, prefix|
          h.map do |k, v|
            case v
            when Hash
              new_prefix = (prefix.to_s || 'SETTINGS')
              new_prefix += "__#{k.upcase}"
              hash_to_vars.call(v, new_prefix)
            else
              "SETTINGS#{prefix}__#{k.upcase}=#{v}"
            end
          end
        end
        hash_to_vars.call(config_file.to_hash).flatten.join(' ')
      end

      def environment
        heroku("run 'echo $RAILS_ENV'").chomp[/(\w+)\z/]
      end

      def heroku(command)
        with_app = app ? " --app #{app}" : ""
        `heroku #{command}#{with_app}`
      end

      def `(command)
        Bundler.with_clean_env { super }
      end
    end
  end
end

namespace 'config' do
  task heroku: :environment do |_, args|
    Config::Integrations::Heroku2.new(args[:app]).invoke
  end
end