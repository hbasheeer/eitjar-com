class RootStoreRoutes
  def self.matches? request
    request.env[:current_store].present?
  end
end