require 'jwt'

class JsonWebToken
  JWT_SECRET_KEY = Rails.application.credentials[Rails.env.to_sym][:jwt][:secret_key]
  JWT_ENCRYPTION_METHOD = Rails.application.credentials[Rails.env.to_sym][:jwt][:encryption_method]
  
  def self.encode(payload, expiration = Settings.remember_for.months.from_now, secret_key = JWT_SECRET_KEY, encryption_method = JWT_ENCRYPTION_METHOD)
    payload = payload.dup
    payload['exp'] = expiration.to_i
    JWT.encode(payload, secret_key, encryption_method)
  end

  def self.decode(token, secret_key = JWT_SECRET_KEY , encryption_method = JWT_ENCRYPTION_METHOD)
    JWT.decode(token, secret_key, encryption_method)
    # raise custom error to be handled by custom handler
    rescue JWT::ExpiredSignature, JWT::VerificationError => e
      raise Errors::ExpiredSignature, e.message
    rescue JWT::DecodeError, JWT::VerificationError => e
      raise Errors::DecodeError, e.message
  end

end
