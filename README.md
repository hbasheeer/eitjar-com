## First Time Installation ##

### Development
- You need to install imagemagick-devel, nodejs, postgres, postgresql-contrib
- Locally:
  - Copy config/database.yml.default to config/database.yml
  - Copy config/settings.yml.default to config/settings.yml
  - Copy config/settings/development.yml.default to config/settings/development.yml
  - Add s3, mail credentials in config/settings/development.yml
- Run: `bundle install`
- Run  `rake db:migrate`
- Run  `rake db:seed`
- Finally Run `rails s`


### Staging
- You need to install imagemagick-devel, nodejs, postgres, postgresql-contrib
- You might want to change the deployer user and server ip in config/deploy/staging.rb
- upload config files to staging: `cap staging setup:init`
- SSH to the server and add the needed credentials to shared/config/settings/staging.yml and shared/config/database.yml
- Deploy on staging: `cap staging deploy`


### Production
- You need to install imagemagick-devel, nodejs, postgres, postgresql-contrib
- You might want to change the deployer user and server ip in config/deploy/production.rb
- upload config files to production: `cap production setup:init`
- SSH to the server and add the needed credentials to shared/config/settings/production.yml and shared/config/database.yml
- Deploy on production: `cap production deploy`


## Updates ##
- Run: `cap #{production or staging} deploy`


### How to add another languege

- in `application_controller.rb`

add this lines

```
  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale || cookies[:locale] 
    @locale = I18n.locale
  end

  def default_url_options(options = {})
     {locale: I18n.locale}.merge options
  end
```
- in 'config/routes.rb'

add this scope 

```
scope ':locale', locale: /#{I18n.available_locales.join("|")}/ do
...
 # rest of routes
...
end
# All paths with no locale specified will be redirected to the same paths with default locale
root to: redirect("/#{I18n.default_locale}", status: 302), as: :redirected_root
get "/*path", to: redirect("/#{I18n.default_locale}/%{path}", status: 302), constraints: {path: /(?!(#{I18n.available_locales.join("|")})\/).*/}, format: false

get '*path', :to => 'errors#not_found', via: :all

```
in 'config/locale.rb'

edit this line

```
I18n.available_locales = [:ar, :en] # array of avaiable locales
 
I18n.default_locale = :ar
```

Finally add translate files to folder `config/locales/**/*.rb`

rememeber to pass `locale: @locale` to every form or link in views

*note:* to switch between languages change the locale only, ex:

```
<%= link_to locale: :en   %>

```


dropzone block
```
<div id="attachmentsUploader" class="dropzone-box">
  <div class="dz-default dz-message dropzone-box-message">
    <div class="media">
      <div class="media-right m-0 pl-15 pr-0">
        <i class="fa fa-fw fa-cloud-upload upload-icon"></i>
      </div>
      <div class="media-body">
          <h3 class="upload-title-drop ">اسحب الملفات هنا</h3>
          <p class="upload-title-click m-0">أو انقر للاختيار يدويا</p>
      </div>
    </div>
  </div>
</div>
```