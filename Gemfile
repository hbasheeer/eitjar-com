source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

gem 'rails', '~> 6.0.0'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5'
gem 'webpacker', '~> 4.0'
gem 'turbolinks', '~> 5'
gem 'font-awesome-rails', '~> 4.6'
gem 'jbuilder', '~> 2.7'
# POSTEGRE DB LIB
gem 'pg', '~> 0.18'
# TOOL FOR CATCHING ERRORS
gem 'sentry-raven'
# Use Active Storage variant
gem 'image_processing', '~> 1.2'
# Active Storage Validations
gem 'active_storage_validations'
# CACHING LIB
gem 'redis'
gem 'redis-rails'
# Mailgun HTTP API
gem 'mailgun_rails'
# GRAPHQL SERVER
gem 'graphql'
# background processing
gem 'sidekiq'
# PAGINATION LIB
gem 'pagy'
# Authorization gem
gem 'pundit'
# multi-environment yaml settings
gem 'config'
# Authentication libs
gem 'jwt'
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
gem "omniauth-google-oauth2"
# TOOL FOR UPLOAD FILES
gem 'carrierwave', '~> 1.0'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

gem "google-cloud-storage", "~> 1.8", require: false

gem 'telegram-bot-ruby'

gem 'rack-cors'
gem 'rack-attack'


gem 'chosen-rails'
gem 'fog-aws', '~> 2'


gem 'gravatar_image_tag'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  gem 'rspec-rails', '~> 3.6'
  gem 'factory_bot_rails'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem "capistrano", "~> 3.14", require: false
  gem 'capistrano-rails'
  gem 'capistrano-rvm', require: false
  gem 'capistrano-db-tasks', require: false 
  gem 'capistrano-bundler', require: false
  gem 'capistrano-yarn'
  gem 'capistrano3-nginx'
  gem 'capistrano3-puma'
  gem 'rails-admin-scaffold'
  gem 'sqlite3', '~> 1.4'
  gem 'graphiql-rails'
end

group :test do
  gem 'shoulda-matchers', require: false
  gem 'database_cleaner', '~> 1.6.2'
  gem 'faker'
end
