FactoryBot.define do
  factory :order do
    note {"MyText"}
    user_id {1}
    lat {1.5}
    lng {1.5}
  end
end
