FactoryBot.define do
  factory :store do
    ident { "MyString" }
    slogan { "MyString" }
    description { "MyText" }
  end
end
