FactoryBot.define do
  factory :location do
    latitude { 1.5 }
    longitude { 1.5 }
    address { "MyString" }
    google_address { "MyString" }
    location_type { "MyString" }
  end
end
