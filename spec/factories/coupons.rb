FactoryBot.define do
  factory :coupon do
    code { "MyString" }
    used_by { 1 }
    used { false }
  end
end
