FactoryBot.define do
  factory :review do
    user_id {1}
    product_id {1}
    rate {1}
    content {"MyText" }
  end
end
