FactoryBot.define do
  factory :store_membership do
    store_id { 1 }
    user_id { 1 }
    status { "MyString" }
    admin { false }
  end
end
