FactoryBot.define do
  factory :product_offer do
    product_id {1}
    start_at {"2018-09-17 11:48:43"}
    end_at {"2018-09-17 11:48:43"}
    price {1}
    active {false}
  end
end
