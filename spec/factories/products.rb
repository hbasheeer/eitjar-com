FactoryBot.define do
  factory :product do
    name {"MyString"}
    description {"MyText"}
    category_id {1}
    price {1}
    status {1}
    available_pices {1}
    image {"MyString"}
  end
end
