FactoryBot.define do
  factory :store_setup_step do
    store_id { 1 }
    slogan_and_description { false }
    products { false }
    cover_photo { false }
  end
end
