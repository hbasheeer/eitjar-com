FactoryBot.define do
  factory :telegram_user do
    chat_id {1}
    first_name {"MyString"}
    last_name {"MyString"}
    username {"MyString"}
    admin {false }
  end
end
