# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

info = {
  fullname: 'حسن بشير',
  email: "hasanbasher1989@gmail.com",
  password: 'hassan12612',
  password_confirmation: 'hassan12612',
  mobile: "0730068483",
  address: "زركته تازة",
  confirmed_at: Time.now
}

user = User.where(email: info[:email]).first || User.create!(info)
user.admin!

puts "Create User" if user

main_categoty = [
  { slug: 'market',           ar_name: 'ماركت',             ku_name: 'market'},
  { slug: 'bakery',           ar_name: 'مخبز',              ku_name: 'خبز' },
  { slug: 'pastry',           ar_name: 'حلويات' ,           ku_name: 'مجتمع ريادة الأعمال' },
  { slug: 'dairy',            ar_name: 'ألبان & وأجبان' ,   ku_name: 'dairy' },
  { slug: 'nuts',             ar_name: 'مكسرات & موالح',    ku_name: 'nuts' },
  { slug: 'detergent',        ar_name: 'منظفات',            ku_name: 'detergent' },
  { slug: 'vegetables',       ar_name: 'خضار' ,             ku_name: 'vegetables' },
]

categories = [
  { parent_id: 1, slug: 'beverages',        ar_name: 'ماء & عصائر', 		   ku_name: 'ماء & مشروبات'},
  { parent_id: 1, slug: 'tea&coffee',       ar_name: 'شاي & قهوة', 			   ku_name: 'شاي & قهوة' },
  { parent_id: 1, slug: 'oil&margarine',    ar_name: 'زيوت & دهن' , 			 ku_name: 'مجتمع التجارة الإلكترونية' },
  { parent_id: 1, slug: 'jam&honey',        ar_name: 'مربى & عسل' , 			 ku_name: 'مجتمع ' },
  { parent_id: 1, slug: 'rice&beans',       ar_name: 'بقوليات & تمن' , 		 ku_name: 'كجتمع العمل الحر' },
  { parent_id: 1, slug: 'flour',            ar_name: 'طحين' ,              ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'vinegar&pickled',  ar_name: 'مخلل & خل' ,         ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'sugar&salt',       ar_name: 'سكر & ملح' ,         ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'spices',           ar_name: 'توابل' ,             ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'preserves',        ar_name: 'معلبات عذائية' ,     ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'dry_fruits',       ar_name: 'فواكه جافة' ,        ku_name: 'مجتمع التسويق الإلكتروني' },
  { parent_id: 1, slug: 'biscuit',          ar_name: 'بسكويت' ,            ku_name: 'كجتمع العمل الحر' },
  { parent_id: 1, slug: 'baby',             ar_name: 'منتجات أطفال',      ku_name: 'كجتمع العمل الحر' },
]

main_categoty.each do |cate|
  next if category = Category.where(slug: cate[:slug]).first
  category = Category.new(slug: cate[:slug])
  category.ar_name = cate[:ar_name]
  category.ku_name = cate[:ku_name]
  category.save
end

puts "Create Main Category"

categories.each do |cate|
  next if category = Category.where(slug: cate[:slug]).first
  category = Category.new(slug: cate[:slug])
  category.ar_name = cate[:ar_name]
  category.ku_name = cate[:ku_name]
  category.parent_id = Category.where(slug: 'market').first.id
  category.save
end

puts "Create SubCategory"

offers = [
  {product_id: 1 , start_at: Time.now , end_at: Time.now + 7.days , price: 250 , active: true },
  {product_id: 2 , start_at: Time.now , end_at: Time.now + 7.days , price: 650 , active: true }
]

offers.each do |p|
  next if offer = ProductOffer.where(product_id: p[:product_id]).first
  offer = ProductOffer.new(product_id: p[:product_id])
  offer.start_at = p[:start_at]
  offer.end_at = p[:end_at]
  offer.price = p[:price]
  offer.active = p[:active]
  offer.save
end

puts "Create Offers"

csv_text = File.read(Rails.root.join('db', 'products.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  next if product = Product.where(title: row['ar_name']).first
  product = Product.new(title: row['ar_name'])
  product.ku_name = row['ku_name']
  product.ar_description = row['ar_description']
  product.ku_description = row['ku_description']
  product.price = row['price']
  product.category_id = row['category_id']
  product.remote_image_url = row['remote_image_url']
  product.in_stack = row['in_stack']
  product.save
end

puts "Create Products"
puts "DONE !!!"


