class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :ident
      t.string :domain
      t.string :slogan
      t.text   :description
      t.integer :owner_id
      t.boolean :use_domain, default: false
      t.boolean :enable_google_maps, default: false
      t.string :google_maps_key
      t.string :google_analytics_key
      t.string :meta_title
      t.string :meta_keywords
      t.text   :meta_description
      t.string :facebook
      t.string :twitter
      t.string :instagram
      t.string :email
      t.string :address
      t.string :mobile
      t.string :currency
      t.integer :min_order_total, default: 0
      t.boolean :enable_reviews, default: true
      t.integer :max_pending_order, default: 5
      t.integer :delivery_amount, default: 0
      t.integer :free_delivery_with_amount, default: 0
      t.string :delivery_start_at, default: 9
      t.string :delivery_end_at, default: 18

      t.timestamps
    end
    add_index :stores, :ident,  unique: true
    add_index :stores, :owner_id,  unique: true
    add_index :stores, :domain, unique: true
  end
end
