class CreateLocations < ActiveRecord::Migration[6.0]
  def change
    create_table :locations do |t|
      t.references :store, foreign_key: true
      t.references :user, foreign_key: true
      t.float :latitude
      t.float :longitude
      t.string :mobile
      t.string :fullname
      t.string :address
      t.string :google_address
      t.string :location_type

      t.timestamps
    end
  end
end
