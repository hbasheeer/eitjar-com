class CreateCoupons < ActiveRecord::Migration[6.0]
  def change
    create_table :coupons do |t|
      t.string :code
      t.integer :used_by
      t.boolean :used, default: false
      t.string  :plan_type
      t.integer :plan_duration, default: 1

      t.timestamps
    end
    add_index :coupons, :code
    add_index :coupons, :used_by
  end
end
