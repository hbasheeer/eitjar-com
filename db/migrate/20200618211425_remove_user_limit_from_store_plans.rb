class RemoveUserLimitFromStorePlans < ActiveRecord::Migration[6.0]
  def change
    remove_column :store_plans, :user_limit, :integer
    remove_column :store_plans, :product_limit, :integer
  end
end
