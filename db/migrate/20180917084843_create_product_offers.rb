class CreateProductOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :product_offers do |t|
      t.integer :product_id
      t.datetime :start_at
      t.datetime :end_at
      t.integer :price
      t.boolean :active, default: true
      t.references :store, null: false, foreign_key: true

      t.timestamps
    end
  end
end
