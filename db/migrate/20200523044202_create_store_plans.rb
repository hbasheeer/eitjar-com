class CreateStorePlans < ActiveRecord::Migration[6.0]
  def change
    create_table :store_plans do |t|
      t.references :store, null: false, foreign_key: true
      t.integer :status
      t.integer  :plan_type
      t.integer  :user_limit
      t.integer  :product_limit
      t.json    :features
      t.datetime  :expires_at
      t.timestamps
    end
  end
end
