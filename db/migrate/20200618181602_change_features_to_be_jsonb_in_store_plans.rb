class ChangeFeaturesToBeJsonbInStorePlans < ActiveRecord::Migration[6.0]
  def change
    change_column :store_plans, :features, :jsonb
  end
end
