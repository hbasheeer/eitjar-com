class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|
      t.string   :unique_id
      t.references :store, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end

    add_index :carts, :unique_id,   unique: true
  end
end
