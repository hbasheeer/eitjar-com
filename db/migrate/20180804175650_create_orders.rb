class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :store, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.references :location, null: false, foreign_key: true
      t.text :detail
      t.integer :order_cart_id
      t.integer :status
      t.text :admin_detail
      t.integer :delivery_price

      t.timestamps
    end
    add_index :orders, :order_cart_id
  end
end
