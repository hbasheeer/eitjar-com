class CreateStoreSubscribeLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :store_subscribe_logs do |t|
      t.references :store, null: false, foreign_key: true
      t.string  :plan_type
      t.string  :payment_method
      t.integer :amount
      t.integer :duration
      t.text    :detail
      t.datetime :expires_at
      t.timestamps
    end
  end
end
