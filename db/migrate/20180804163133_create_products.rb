class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.references :store, null: false, foreign_key: true
      t.references :category, null: false, foreign_key: true
      t.string :title
      t.text :description
      t.integer :price
      t.integer :status
      t.integer :in_stack, default: 0
      t.boolean :is_infinite
      t.boolean :featured
      
      t.timestamps
    end
  end
end
