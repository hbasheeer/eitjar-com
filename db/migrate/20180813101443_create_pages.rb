class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.references :store, null: false, foreign_key: true
      t.string :title
      t.text :content
      t.string :slug
      t.string :meta_keywords
      t.text :meta_description
      t.boolean :published

      t.timestamps
    end
    add_index :pages, [:store_id, :slug],   unique: true
  end
end
