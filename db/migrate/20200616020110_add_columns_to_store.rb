class AddColumnsToStore < ActiveRecord::Migration[6.0]
  def change
    add_column :stores, :users_count, :integer, default: 0
    add_column :stores, :categories_count, :integer, default: 0
    add_column :stores, :orders_count, :integer, default: 0
    add_column :stores, :products_count, :integer, default: 0
    add_column :stores, :pages_count, :integer, default: 0
    add_column :stores, :reviews_count, :integer, default: 0
  end
end
