class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.references :store, null: false, foreign_key: true
      t.string :name
      t.integer :parent_id
      t.string :slug
      t.string :image
      t.boolean :hidden, default: false

      t.timestamps
    end
    add_index :categories, :parent_id
  end
end
