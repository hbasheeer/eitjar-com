class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.references :store, null: false, foreign_key: true
      t.string :name
      t.string :email
      t.text :message

      t.timestamps
    end
  end
end
