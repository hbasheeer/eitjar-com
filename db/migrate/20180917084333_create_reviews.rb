class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.references :product, foreign_key: true
      t.references :user, foreign_key: true
      t.references :store, null: false, foreign_key: true
      t.boolean    :deleted, default: false
      t.boolean    :hidden, default: false
      t.integer    :rate
      t.text       :content

      t.timestamps
    end
  end
end
