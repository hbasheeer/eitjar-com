class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.references :product, foreign_key: true
      t.references :order, foreign_key: true
      t.references :cart, foreign_key: true
      t.references :store, null: false, foreign_key: true
      t.integer  :quantity, default: 1
      t.integer  :price

      t.timestamps
    end
  end
end
