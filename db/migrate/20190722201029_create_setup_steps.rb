class CreateSetupSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :setup_steps do |t|
      t.references :store, null: false, foreign_key: true
      t.boolean :slogan_and_description, default: false
      t.boolean :products, default: false
      t.boolean :visual_identity, default: false
      t.boolean :configuration, default: false

      t.timestamps
    end
  end
end
