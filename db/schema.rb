# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_18_211425) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cart_items", force: :cascade do |t|
    t.integer "product_id"
    t.integer "order_id"
    t.integer "cart_id"
    t.integer "store_id", null: false
    t.integer "quantity", default: 1
    t.integer "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cart_id"], name: "index_cart_items_on_cart_id"
    t.index ["order_id"], name: "index_cart_items_on_order_id"
    t.index ["product_id"], name: "index_cart_items_on_product_id"
    t.index ["store_id"], name: "index_cart_items_on_store_id"
  end

  create_table "carts", force: :cascade do |t|
    t.string "unique_id"
    t.integer "store_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_carts_on_store_id"
    t.index ["unique_id"], name: "index_carts_on_unique_id", unique: true
    t.index ["user_id"], name: "index_carts_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.integer "store_id", null: false
    t.string "name"
    t.integer "parent_id"
    t.string "slug"
    t.string "image"
    t.boolean "hidden", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parent_id"], name: "index_categories_on_parent_id"
    t.index ["store_id"], name: "index_categories_on_store_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.integer "store_id", null: false
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_contacts_on_store_id"
  end

  create_table "coupons", force: :cascade do |t|
    t.string "code"
    t.integer "used_by"
    t.boolean "used", default: false
    t.string "plan_type"
    t.integer "plan_duration", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_coupons_on_code"
    t.index ["used_by"], name: "index_coupons_on_used_by"
  end

  create_table "favorites", force: :cascade do |t|
    t.integer "user_id"
    t.integer "product_id"
    t.integer "store_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_favorites_on_product_id"
    t.index ["store_id"], name: "index_favorites_on_store_id"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "identities", force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.integer "user_id", null: false
    t.integer "store_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["store_id"], name: "index_identities_on_store_id"
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "locations", force: :cascade do |t|
    t.integer "store_id"
    t.integer "user_id"
    t.float "latitude"
    t.float "longitude"
    t.string "mobile"
    t.string "fullname"
    t.string "address"
    t.string "google_address"
    t.string "location_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["store_id"], name: "index_locations_on_store_id"
    t.index ["user_id"], name: "index_locations_on_user_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "product_id"
    t.integer "cart_id"
    t.integer "order_id", null: false
    t.integer "store_id", null: false
    t.integer "quantity", default: 1
    t.integer "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cart_id"], name: "index_order_items_on_cart_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
    t.index ["store_id"], name: "index_order_items_on_store_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "store_id", null: false
    t.integer "user_id", null: false
    t.integer "location_id", null: false
    t.text "detail"
    t.integer "order_cart_id"
    t.integer "status"
    t.text "admin_detail"
    t.integer "delivery_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_id"], name: "index_orders_on_location_id"
    t.index ["order_cart_id"], name: "index_orders_on_order_cart_id"
    t.index ["store_id"], name: "index_orders_on_store_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "pages", force: :cascade do |t|
    t.integer "store_id", null: false
    t.string "title"
    t.text "content"
    t.string "slug"
    t.string "meta_keywords"
    t.text "meta_description"
    t.boolean "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id", "slug"], name: "index_pages_on_store_id_and_slug", unique: true
    t.index ["store_id"], name: "index_pages_on_store_id"
  end

  create_table "product_images", force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "store_id", null: false
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_images_on_product_id"
    t.index ["store_id"], name: "index_product_images_on_store_id"
  end

  create_table "product_offers", force: :cascade do |t|
    t.integer "product_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.integer "price"
    t.boolean "active", default: true
    t.integer "store_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_product_offers_on_store_id"
  end

  create_table "products", force: :cascade do |t|
    t.integer "store_id", null: false
    t.integer "category_id", null: false
    t.string "title"
    t.text "description"
    t.integer "price"
    t.integer "status"
    t.integer "in_stack", default: 0
    t.boolean "is_infinite"
    t.boolean "featured"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["store_id"], name: "index_products_on_store_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "product_id"
    t.integer "user_id"
    t.integer "store_id", null: false
    t.boolean "deleted", default: false
    t.boolean "hidden", default: false
    t.integer "rate"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_reviews_on_product_id"
    t.index ["store_id"], name: "index_reviews_on_store_id"
    t.index ["user_id"], name: "index_reviews_on_user_id"
  end

  create_table "setup_steps", force: :cascade do |t|
    t.integer "store_id", null: false
    t.boolean "slogan_and_description", default: false
    t.boolean "products", default: false
    t.boolean "visual_identity", default: false
    t.boolean "configuration", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_setup_steps_on_store_id"
  end

  create_table "store_plans", force: :cascade do |t|
    t.integer "store_id", null: false
    t.integer "status"
    t.integer "plan_type"
    t.json "features"
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["store_id"], name: "index_store_plans_on_store_id"
  end

  create_table "store_subscribe_logs", force: :cascade do |t|
    t.integer "store_id", null: false
    t.string "plan_type"
    t.string "payment_method"
    t.integer "amount"
    t.integer "duration"
    t.text "detail"
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["store_id"], name: "index_store_subscribe_logs_on_store_id"
  end

  create_table "stores", force: :cascade do |t|
    t.string "ident"
    t.string "domain"
    t.string "slogan"
    t.text "description"
    t.integer "owner_id"
    t.boolean "use_domain", default: false
    t.boolean "enable_google_maps", default: false
    t.string "google_maps_key"
    t.string "google_analytics_key"
    t.string "meta_title"
    t.string "meta_keywords"
    t.text "meta_description"
    t.string "facebook"
    t.string "twitter"
    t.string "instagram"
    t.string "email"
    t.string "address"
    t.string "mobile"
    t.string "currency"
    t.integer "min_order_total", default: 0
    t.boolean "enable_reviews", default: true
    t.integer "max_pending_order", default: 5
    t.integer "delivery_amount", default: 0
    t.integer "free_delivery_with_amount", default: 0
    t.string "delivery_start_at", default: "9"
    t.string "delivery_end_at", default: "18"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "users_count", default: 0
    t.integer "categories_count", default: 0
    t.integer "orders_count", default: 0
    t.integer "products_count", default: 0
    t.integer "pages_count", default: 0
    t.integer "reviews_count", default: 0
    t.index ["domain"], name: "index_stores_on_domain", unique: true
    t.index ["ident"], name: "index_stores_on_ident", unique: true
    t.index ["owner_id"], name: "index_stores_on_owner_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "fullname"
    t.string "username"
    t.string "mobile"
    t.string "country"
    t.string "address"
    t.integer "role"
    t.integer "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["store_id", "email"], name: "index_users_on_store_id_and_email", unique: true
    t.index ["store_id"], name: "index_users_on_store_id"
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "orders"
  add_foreign_key "cart_items", "products"
  add_foreign_key "cart_items", "stores"
  add_foreign_key "carts", "stores"
  add_foreign_key "carts", "users"
  add_foreign_key "categories", "stores"
  add_foreign_key "contacts", "stores"
  add_foreign_key "favorites", "products"
  add_foreign_key "favorites", "stores"
  add_foreign_key "favorites", "users"
  add_foreign_key "identities", "stores"
  add_foreign_key "identities", "users"
  add_foreign_key "locations", "stores"
  add_foreign_key "locations", "users"
  add_foreign_key "order_items", "carts"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "products"
  add_foreign_key "order_items", "stores"
  add_foreign_key "orders", "locations"
  add_foreign_key "orders", "stores"
  add_foreign_key "orders", "users"
  add_foreign_key "pages", "stores"
  add_foreign_key "product_images", "products"
  add_foreign_key "product_images", "stores"
  add_foreign_key "product_offers", "stores"
  add_foreign_key "products", "categories"
  add_foreign_key "products", "stores"
  add_foreign_key "reviews", "products"
  add_foreign_key "reviews", "stores"
  add_foreign_key "reviews", "users"
  add_foreign_key "setup_steps", "stores"
  add_foreign_key "store_plans", "stores"
  add_foreign_key "store_subscribe_logs", "stores"
  add_foreign_key "users", "stores"
end
