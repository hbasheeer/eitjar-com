class ImageUploader < BaseUploader

  version :thumb do
    process resize_to_fit: [200, 200]
  end

  version :small do 
    process :resize_to_limit => [80,80] 
  end

end
