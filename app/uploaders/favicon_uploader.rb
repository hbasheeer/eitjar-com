class FaviconUploader < BaseUploader

  version :favicon do
    process resize_to_fit: [32, 32]
  end

end
