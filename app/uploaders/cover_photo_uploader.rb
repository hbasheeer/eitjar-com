class CoverPhotoUploader < BaseUploader

  version :hd_header do
    process resize_to_fit: [2048, 614]
  end

  version :header do
    process resize_to_fit: [1600, 400]
  end

end
