class LogoUploader < BaseUploader

  version :header do
    process resize_to_fit: [192, 192]
  end

  version :paypal do
    process resize_to_fit: [190, 60]
  end

end
