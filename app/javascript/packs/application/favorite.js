import MarketPlace from './app';

MarketPlace.favorite = function(){

  function init(){
    _favorite_actions();
  }

  function _favorite_actions(){
    $('.delete_fav').click(function(e) {
      e.preventDefault();
      $('#delete_fav').data('id', $(this).data('id')).modal('show');
      var $submit = $('#delete_fav').find('.btn-primary');
      href = $submit.attr('href');
      $submit.attr('href', href.replace('product_id', $(this).data('id')));
    });

    $('.delete_item').click(function(e) {
      e.preventDefault();
      $('#delete_item').data('id', $(this).data('id')).modal('show');
      var $submit = $('#delete_item').find('.btn-primary');
      href = $submit.attr('href');
      $submit.attr('href', href.replace('item_id', $(this).data('id')));
    });
  }

  return {
    init: init
  };
}();