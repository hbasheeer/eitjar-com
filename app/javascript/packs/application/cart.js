import MarketPlace from './app';

MarketPlace.shopping_cart = function(){

  function init(){
    _cart_actions();
    _cart_box();
  }

  function _cart_actions(){
    $('.cart_quantity_up').bind('ajax:beforeSend', function() {
      $(this).children('.fa-plus-square').addClass('hidden');
      $(this).children('.fa-spinner').removeClass('hidden');
    }).bind('ajax:complete', function() {
      $(this).children('.fa-plus-square').removeClass('hidden');
      $(this).children('.fa-spinner').addClass('hidden');
    });

    $('.cart_quantity_down').bind('ajax:beforeSend', function() {
      $(this).children('.fa-minus-square').addClass('hidden');
      $(this).children('.fa-spinner').removeClass('hidden');
    }).bind('ajax:complete', function() {
      $(this).children('.fa-minus-square').removeClass('hidden');
      $(this).children('.fa-spinner').addClass('hidden');
    });

    $('.add_to_cart').bind('ajax:beforeSend', function() {
      $(this).children('.fa-cart-plus').addClass('hidden');
      $(this).children('.fa-spinner').removeClass('hidden');
    });

    $('a.delete-item').click(function(){
      $(this).children('i.fa-times').addClass('hidden');
        $(this).children('i.fa-spin').removeClass('hidden');
    });
  }

  function _cart_box(){
    // Srearch
    $('li.search i.fa').click(function () {
      if($('#header .search-box').is(":visible")) {
        $('#header .search-box').fadeOut(300);
      } else {
        $('.search-box').fadeIn(300);
        $('#header .search-box form input').focus();

        // hide quick cart if visible
        if ($('#header li.quick-cart div.quick-cart-box').is(":visible")) {
          $('#header li.quick-cart div.quick-cart-box').fadeOut(300);
        }
      }
    }); 

    // close search box on body click
    if($('#header li.search i.fa')) {
      $('#header .search-box, #header li.search i.fa').on('click', function(e){
        e.stopPropagation();
      });

      $('body').on('click', function() {
        if($('#header li.search .search-box').is(":visible")) {
          $('#header .search-box').fadeOut(300);
        }
      });
    }

    $(document).bind("click", function() {
      if($('#header li.search .search-box').is(":visible")) {
        $('#header .search-box').fadeOut(300);
      }
    });


    // Close Fullscreen Search
    $("#closeSearch").bind("click", function(e) {
      e.preventDefault();

      $('#header .search-box').fadeOut(300);
    });



    // Page Menu [mobile]
    $("button#page-menu-mobile").bind("click", function() {
      $(this).next('ul').slideToggle(150);
    });


    // Quick Cart
    $('li.quick-cart>a').click(function (e) {
      e.preventDefault();
      
      var _quick_cart_box = $('li.quick-cart div.quick-cart-box');

      if(_quick_cart_box.is(":visible")) {
        _quick_cart_box.fadeOut(300);
      } else {
        _quick_cart_box.fadeIn(300);

        // close search if visible
        if($('li.search .search-box').is(":visible")) {
          $('.search-box').fadeOut(300);
        }
      }
    });
    // close quick cart on body click
    if($('li.quick-cart>a')) {
      $('li.quick-cart').on('click', function(e){
        e.stopPropagation();
      });

      $('body').on('click', function() {
        if ($('li.quick-cart div.quick-cart-box').is(":visible")) {
          $('li.quick-cart div.quick-cart-box').fadeOut(300);
        }
      });
    }
  }

  return {
    init: init
  };
}();
