import MarketPlace from './app';

MarketPlace.Uploader = function(){

  function init_fileinput(){
    $( ".products-images" ).change(function() {
      $("#photos").html('');
      var input = this;
      var counter = input.files.length;
      var files_num = counter > 0 ? counter + ' ملف تم اختيارهم' : null;
      if(files_num) {$('.upload-info').val(files_num);}

      for(var x = 0; x<counter; x++){
        if (input.files && input.files[x]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="'+e.target.result+'" class="img-thumbnail"></div>');
          };
          reader.readAsDataURL(input.files[x]);
        }
      }
    });

    $( ".upload-file" ).change(function() {
      var preview = $(this).closest('.row').children().children('.preview-image')
      preview.html('');
      var input = this;
      var counter = input.files.length;
      var files_num = counter > 0 ? counter + ' ملف تم اختياره' : null;
      var upload_info = $(this).closest('.input-group').children('.upload-info')
      if(files_num) { upload_info.val(files_num);}

        if (input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            preview.append('<img src="'+e.target.result+'" class="img-thumbnail">');
          };
          reader.readAsDataURL(input.files[0]);
        }
    });
  }

  function init(){
    init_fileinput();
  };

  return {
    init: init
  }
}();
