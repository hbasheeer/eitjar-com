import MarketPlace from './app';

MarketPlace.onTurboLoad = function () {
  MarketPlace.shopping_cart.init();
  MarketPlace.Uploader.init();
  MarketPlace.RichText.init();
  MarketPlace.plugins.init();
  MarketPlace.header.init();
};

MarketPlace.onPageLoad = function () {
  MarketPlace.order.init();
  MarketPlace.favorite.init();
  MarketPlace.Uploader.init();
  MarketPlace.plugins.init();
  MarketPlace.header.init();
  MarketPlace.shopping_cart.init();
  MarketPlace.RichText.init();
  MarketPlace.map.init();
  $('[data-toggle="tooltip"]').tooltip();
  /** Preloader
  **************************************************************** **/
  if($('#preloader').length > 0) {
    $('#preloader').fadeOut(3000, function() {
      $('#preloader').remove();
    });
  }

};


$(window).on('load', MarketPlace.onPageLoad )
