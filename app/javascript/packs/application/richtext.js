import MarketPlace from './app';
import summernote from 'summernote';

MarketPlace.RichText = function(){

  function init(){
    _init_summernote();
  }

  function _init_summernote(){
    $('.textarea_editor').summernote({
        toolbar: [
          [ 'style', [ 'style' ] ],
          [ 'font', [ 'bold', 'italic', 'underline', 'clear'] ],
          [ 'fontname', [ 'fontname' ] ],
          [ 'color', [ 'color' ] ],
          [ 'para', [ 'ol', 'ul', 'paragraph' ] ],
          [ 'table', [ 'table' ] ],
          [ 'insert', [ 'link'] ],
          [ 'view', [ 'undo', 'redo', 'fullscreen' ] ]
        ]
    });
  }

  return {
    init: init
  };

}();