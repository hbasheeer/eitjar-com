import MarketPlace from './app';

MarketPlace.order = function(){

  function init(){
    _order_actions();
  }

  function _order_actions(){
    $('.cancel_order_button').click(function(e) {
      e.preventDefault();
      $('#cancel_order').data('id', $(this).data('id')).modal('show');
      var $submit = $('#cancel_order').find('.btn-primary');
      href = $submit.attr('href');
      $submit.attr('href', href.replace('order_id', $(this).data('id')));
    });
  }

  return {
    init: init
  };
}();
