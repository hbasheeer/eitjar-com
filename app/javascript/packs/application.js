require("@rails/ujs").start()
//require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
window.jQuery = $;
window.$ = $;
// libs
require('jquery')
require('bootstrap-sass');
require('jquery-zoom')
require('./application/libs');
require('./application/favorite');
require('./application/order');
require('./application/cart');
require('./application/header');
require('./application/maps.js.erb');
require('./application/uploader');
require('./application/plugins');
require('./application/richtext');
require('./application/boot');
require('./application/scripts');