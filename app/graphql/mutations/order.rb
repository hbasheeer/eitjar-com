module Mutations
  module Order 
  	
  	class Create < Mutations::Base
      argument :attributes, Types::OrderInputsType, required: true

      field :order, Types::OrderType, null: false

      def resolve(attributes:)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        
        new_order = ::Order.new(attributes.to_h)
        begin
          order = ::MarketPlace::Order.create(new_order, context[:current_user].cart, context)[:order]
          { order: order}
        rescue ::MarketPlace::Errors::UnprocessableEntity
          raise GraphQL::ExecutionError.new(new_order.errors.messages.values.join(', '))
        end
      end
  	end

  	class Cancel < Mutations::Base
  	  argument :id, ID, required: true
  	  
      def resolve(attributes:)
        
      end
  	end
  end
end