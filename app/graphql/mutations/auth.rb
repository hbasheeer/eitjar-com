module Mutations
  module Auth
  	class SignIn < Mutations::Base
  	  argument :attributes, Types::SignInInputsType, required: true

  	  field :user, Types::UserType, null: true

      def resolve(attributes:)
        raise MarketPlace::Api::Errors::InvalidEmailOrPassword unless context[:current_store].present?
        user = ::User.find_for_database_authentication(:email=> attributes[:email], :store_id => context[:current_store].try(:id))
        if user.present? && user.valid_password?(attributes[:password])
          { user: user }
        else
          raise MarketPlace::Api::Errors::InvalidEmailOrPassword
        end
      end
  	end

  	class SignUp < Mutations::Base
  	  argument :attributes, Types::SignUpInputsType, required: true

  	  field :user, Types::UserType, null: true

      def resolve(attributes:)
        terms = attributes[:terms] || true
        user = User.new(attributes.to_h)
  		  if terms && user.save
      	  { user: user }
        else
  		    raise GraphQL::ExecutionError.new(user.errors.full_messages.join(', '))
        end
      end
  	end

    class ResetPassword < Mutations::Base
      argument :email, String, required: true

      field :email, String, null: true
      field :info, String, null: true

      def resolve(email:)
        user = User.where(email: email).first
        if user
          user.send_reset_password_instructions
          {email: email, info: I18n.t("devise.passwords.send_instructions")}
        else
          MarketPlace::Api::Errors::InvalidEmail
        end
      end
    end
  end
end