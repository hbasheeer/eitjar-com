module Mutations
  module Account 
  	class Update < Mutations::Base
	  argument :attributes, Types::UserInputsType, required: true
  	  field :user, Types::UserType, null: false

      def resolve(attributes:)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        
        begin
          user = ::MarketPlace::User.update(attributes.to_h, context)[:user]
            { user: user }
        rescue ::MarketPlace::Errors::UnprocessableEntity
          raise GraphQL::ExecutionError.new(user.errors.messages.values.join(', '))
        end
      end
  	end
  end
end