module Mutations
  module Favorite 
  	
  	class Add < Mutations::Base
  	  argument :product_id, ID, required: true

  	  field :favorite, Types::FavoriteType, null: true

      def resolve(product_id:)
      	raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        product = Product.find(product_id)

        if favorite = ::MarketPlace::Favorite.create(product, context)[:favorite]
          { favorite: favorite }
        else
          { favorite: nil }
        end
      end

  	end

  	class Remove < Mutations::Base
	  argument :product_id, ID, required: true

  	  field :favorites, [Types::FavoriteType], null: true

      def resolve(product_id:)
      	raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        product = Product.find(product_id)
        
        ::MarketPlace::Favorite.destroy(product, context)[:favorite]
        { favorites: context[:current_user].favorites }
      end
  	end
  end
end