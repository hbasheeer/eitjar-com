module Mutations
  module Cart
  	class AddProduct < Mutations::Base
  	  argument :product_id, ID, required: true

  	  field :cart, Types::CartType, null: false

      def resolve(product_id:)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        cart = context[:current_user].cart
        product = ::Product.find(product_id)
        raise MarketPlace::Api::Errors::InvalidProductID unless product
        line_item = cart.add_product(product, quantity =1)
        line_item.save
         { cart: cart }
      end
  	end

    class RemoveProduct < Mutations::Base
      argument :product_id, ID, required: true

      field :cart, Types::CartType, null: false

      def resolve(product_id:)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        cart = context[:current_user].cart
        product = ::Product.find(product_id)
        raise MarketPlace::Api::Errors::InvalidProductID unless product
        line_item = cart.remove_product(product)
        line_item.save
      	 { cart: cart }
      end
  	end

    class DeleteProduct < Mutations::Base
      argument :product_id, ID, required: true

      field :cart, Types::CartType, null: false

      def resolve(product_id:)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        cart = context[:current_user].cart
        product = ::Product.find(product_id)
        raise MarketPlace::Api::Errors::InvalidProductID unless product
        line_item = cart.line_items.where(product_id: product_id).first
        line_item.destroy
        { cart: cart }
      end
    end

  end
end