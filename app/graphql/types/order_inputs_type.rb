module Types
  class OrderInputsType < Types::BaseInputObject 

    #argument :lat, Float, required: true
    #argument :lng, Float, required: true
    argument :detail, String, required: false
    #argument :mobile, Integer, required: true
    #argument :address, String, required: true
  end
end
