module Types
  class ProductType < Types::BaseObject

	field :id, ID, null: false 
	field :title, String, null: false 
	field :description, String, null: true
	field :price, Integer, null: false
	field :delivery_price, String, null: false
	field :image_url, String, null: false
	field :status, Types::ProductStatusType, null: false
	field :category, Types::CategoryType, null: false
	field :in_stack, Integer, null: true
	field :featured, Boolean, null: true
	field :created_at, String, null: false 
	field :updated_at, String, null: false

	def image_url
	  object.image_url
	end
	
	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

  end
end
