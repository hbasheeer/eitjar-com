module Types
  class FavoriteType < Types::BaseObject

	field :id, ID, null: false 
	field :user, Types::UserType, null: false 
	field :product, Types::ProductType, null: false 
	field :created_at, String, null: false 
	field :updated_at, String, null: false
	
	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

  end
end


