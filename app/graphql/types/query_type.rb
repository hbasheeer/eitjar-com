module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.
    
    field :me, resolver: Resolvers::User::Me
    field :user, resolver: Resolvers::User::GetUser

    field :products, resolver: Resolvers::Product::AllProducts
    field :product, resolver: Resolvers::Product::GetProduct
    
    field :categories, resolver: Resolvers::Category::Categories

    field :orders, resolver: Resolvers::Order::AllOrders
    field :order, resolver: Resolvers::Order::GetOrder

    field :get_cart, resolver: Resolvers::Cart::GetCart

  end
end
