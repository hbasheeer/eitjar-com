module Types
  class SignUpInputsType < Types::BaseInputObject 

    argument :fullname, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: true
    argument :address, String, required: false
    argument :mobile, Int, required: false
    argument :terms, Boolean ,required: true , prepare: ->(accept_agreement, ctx) {
      return MarketPlace::Api::Errors::AcceptAgreement if accept_agreement != true
      true
    }
  end
end