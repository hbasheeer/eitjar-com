module Types
  class OrderStatusType < Types::BaseEnum
  	value "executing"
  	value "completed"
  	value "cancelled"
  end
end