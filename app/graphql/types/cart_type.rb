module Types
  class CartType < Types::BaseObject 

	field :id, ID, null: false 
	field :user, Types::UserType, null: true
	field :price, Integer, null: false
	field :total_price, Integer, null: false
	field :delivery_price,Integer, null: false
	field :created_at, String, null: false 
	field :updated_at, String, null: false
	field :line_items, [Types::LineItemType], null: true


	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

	def orders
	  object.orders
	end

	def user
	  object.user
	end

	def line_items
	  object.items
	end

	def price
      object.price
	end

	def total_price
      object.total_price
	end
  end
end
