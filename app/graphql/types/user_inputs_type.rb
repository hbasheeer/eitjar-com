module Types
  class UserInputsType < Types::BaseInputObject 
  	
    argument :fullname, String, required: true
    argument :email, String, required: true
    argument :mobile, Integer, required: false
    argument :address, String, required: false
  end
end
