module Types
  class MutationType < Types::BaseObject

    field :signin, mutation: Mutations::Auth::SignIn
    field :signup, mutation: Mutations::Auth::SignUp
    field :reset_password, mutation: Mutations::Auth::ResetPassword

    field :update_account, mutation: Mutations::Account::Update
    
    field :create_order, mutation: Mutations::Order::Create
    field :cancel_order, mutation: Mutations::Order::Cancel

    field :add_favorite, mutation: Mutations::Favorite::Add
    field :remove_favorite, mutation: Mutations::Favorite::Remove

    field :cart_add_product, mutation: Mutations::Cart::AddProduct
    field :cart_remove_product, mutation: Mutations::Cart::RemoveProduct
    field :cart_delete_product, mutation: Mutations::Cart::DeleteProduct
  end
end
