module Types
  class CategoryType < Types::BaseObject 

	field :id, ID, null: false 
	field :name, String, null: false 
	field :slug, String, null: false 
	field :created_at, String, null: false
	#field :image_url, String, null: true 
	field :updated_at, String, null: false
	#field :subcategories, [Types::CategoryType], null: true
	field :parent, Types::CategoryType, null: true

	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

	def subcategories
	  return [] if !object.has_child?
	  object.subcategories
	end

	def parent
	  object.parent_category
	end

	def image_url
      object.image_url
	end

  end
end