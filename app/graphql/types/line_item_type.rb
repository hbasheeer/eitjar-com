module Types
  class LineItemType < Types::BaseObject 

	field :id, ID, null: false 
	field :order_id, ID, null: false 
	field :quantity, Integer, null: false
	field :price, Integer, null: false 
	field :total_price, Integer, null: false
	field :product, Types::ProductType, null: false
	field :created_at, String, null: false 
	field :updated_at, String, null: false

	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

    def total_price
	  object.price * object.quantity
    end

  end
end