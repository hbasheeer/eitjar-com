module Types
  class ProductStatusType < Types::BaseEnum
  	value "active"
  	value "hidden"
  end
end
