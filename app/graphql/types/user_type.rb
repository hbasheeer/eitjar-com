module Types
  class UserType < Types::BaseObject 

	field :id, ID, null: false 
	field :fullname, String, null: false 
	field :email, String, null: false 
	field :mobile, String, null: true  
	field :created_at, String, null: false 
	field :updated_at, String, null: false
	field :orders, [Types::OrderType], null: true
	field :token, String, null: false

	def created_at
	  object.created_at.utc.iso8601.to_s
	end

	def updated_at
	  object.updated_at.utc.iso8601.to_s
	end

	def orders
	  object.orders
	end

	def token
	  object.token
	end
  end
end
