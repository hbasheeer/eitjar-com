module Types
  class OrderType < Types::BaseObject 

	field :id, ID, null: false 
	field :price, String, null: false
	field :delivery_price, String, null: false
	field :total_price, String, null: false
	field :status, Types::OrderStatusType, null: false
	field :detail, String, null: true
	field :created_at, String, null: false 
	field :updated_at, String, null: false
	field :items, [Types::LineItemType], null: false
	field :user, Types::UserType, null: false

	def created_at
	  object.created_at.strftime("%F")
	end

	def updated_at
	  object.updated_at.strftime("%F")
	end

	def price
	  object.price
	end

	def items
	  object.items
	end

	def delivery_price
	  object.delivery_price
	end

	def user 
    object.user
	end

	def total_price
	  object.total_price
	end

  end
end