module  Resolvers
  module Cart
    class GetCart < Resolvers::Base
      type Types::CartType, null: true

      argument :id, ID, required: false

      def resolve(id: nil)
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user]
        context[:current_user].cart
      end
    end
  end
end
