module Resolvers
  module User
    class Me < Resolvers::Base
      type Types::UserType, null: false

      def authorized?
        raise MarketPlace::Api::Errors::Unauthorized unless context[:current_user].present? 
        true
      end

      def resolve
        context[:current_user]
      end
    end

    class GetUser < Resolvers::Base
      type Types::UserType, null: false

      argument :id, ID, required: true

      def resolve(id:)
        MarketPlace::User.show(id, context)[:user]
      end
    end

  end
end