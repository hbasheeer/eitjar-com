module  Resolvers
  module Product
    class AllProducts < Resolvers::Base
      type [Types::ProductType], null: false

      argument :category_id, ID, required: false

      def resolve(category_id: nil)
        @category = category_id.present? ? ::Category.find(category_id) : nil
        MarketPlace::Product.index(@category, context)[:products]
      end
    end

    class GetProduct < Resolvers::Base
      type Types::ProductType, null: false

      argument :id, ID, required: false

      def resolve(id:)
        MarketPlace::Product.show(id, context)[:product]
      end
    end
  end
end