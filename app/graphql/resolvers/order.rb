module Resolvers
  module Order
    class AllOrders < Resolvers::Base
      type [Types::OrderType], null: false

      argument :status, Types::OrderStatusType, required: false

      def resolve(status: nil)
        MarketPlace::Order.index(status, context)[:orders]
      end
    end

    class GetOrder < Resolvers::Base
      type Types::OrderType, null: false

      argument :id, ID, required: false

      def resolve(id:)
        MarketPlace::Order.show(id, context)[:order]
      end
    end
  end
end