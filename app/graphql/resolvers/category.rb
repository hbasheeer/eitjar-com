module  Resolvers
  module Category
    class Categories < Resolvers::Base
      type [Types::CategoryType], null: false

      argument :category_id, ID, required: false

      def resolve(category_id: nil)
        MarketPlace::Category.index(context)[:categories]
      end
    end
  end
end
