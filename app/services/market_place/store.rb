module MarketPlace
  class Store < ServiceBase
    extend StoresHelper
    RESERVED_DOMAINS = [
      "www",
      "www0",
      "www1",
      "www2",
      "www3",
      "www4",
      "www5",
      "www6",
      "www7",
      "www8",
      "www9",
      "www10",
      "wwww",
      "home",
      "sharetribe",
      "login",
      "blog",
      "business",
      "catch",
      "webhooks",
      "dashboard",
      "dashboardtranslate",
      "translate",
      "community",
      "wiki",
      "mail",
      "secure",
      "host",
      "feed",
      "feeds",
      "app",
      "beta-site",
      "marketplace",
      "marketplaces",
      "masters",
      "marketplacemasters",
      "insights",
      "insight",
      "tips",
      "doc",
      "docs",
      "support",
      "team",
      "support-team",
      "help",
      "legal",
      "org",
      "net",
      "web",
      "intra",
      "intranet",
      "internal",
      "join",
      "job",
      "jobs",
      "career",
      "careers",
      "journey",
      "journeys",
      "webinar",
      "local",
      "marketplace-academy",
      "academy-proxy",
      "academy",
      "proxy",
      "preproduction",
      "staging",
      "demo",
      "plan",
      "plans",
      "customer",
      "customers",
      "subscription",
      "subscriptions",
      "client",
      "clients",
      "assets",
      "assets-origin",
      "assets-sharetribecom",
      "assets0",
      "assets1",
      "assets2",
      "assets3",
      "assets4",
      "assets5",
      "assets6",
      "assets7",
      "assets8",
      "assets9",
      "cdn",
      "cdn-origin",
      "cdn0",
      "cdn1",
      "cdn2",
      "cdn3",
      "cdn4",
      "cdn5",
      "cdn6",
      "cdn7",
      "cdn8",
      "cdn9",
      "flex",
      "api",
      "console",
      "custom",
      "core",
      "turnkey",
      "admin",
    ]

    def available_ident_based_on(initial_ident)
      current_ident = Maybe(initial_ident).to_url[0..29].or_else("trial_site") #truncate to 30 chars or less

      # use basedomain as basis on new variations if current domain is not available
      base_ident = current_ident

      i = 1
      while RESERVED_DOMAINS.include?(current_ident)
        current_ident = "#{base_ident}#{i}"
        i += 1
      end

      return current_ident
    end

    def self.handle_new_store(store)
      store.create_setup_step
      self.find_or_create_plan(store.id)      
    end

    def self.find_or_create_plan(store_id)
      ::StorePlan.find_or_create_by(store_id: store_id) do |plan|
        plan.features = ::StorePlan::FREE_PLAN_FEATURES
      end
    end

  end
end
