module MarketPlace
  class Location < ServiceBase

    def self.index(options)
      user, current_store, params, request = prepare options

      locations = user.locations

      MarketPlace::Result::Success.new locations: locations
    end


    def self.create(location, options)
      user, current_store, params, request = prepare options

      location.user_id = user.id

      raise MarketPlace::Errors::UnprocessableEntity unless location.save

      MarketPlace::Result::Success.new location: location
    end

    def self.update(location, location_params, options)
      user, current_store, params, request = prepare options


      raise MarketPlace::Errors::UnprocessableEntity unless location.update(location_params)

      MarketPlace::Result::Success.new location: location
    end
  end
end
