module MarketPlace
  module Result

    class Result
      # if result is successfull, result must be an object containing one or more results
      attr_accessor :result, :error

      def initialize(result:, error: nil)
        @result, @error = result, error
      end

      def success?
        !@result.nil? && !error?
      end

      def error?
        !@error.nil?
      end

      def [](key)
        @result[key]
      end
    end

    class Success < Result
      def initialize(result = {success: true})
        @result = result
      end
    end

    class Failure < Result
      def initialize(error = true)
        @error = error
      end
    end

  end
end
