module MarketPlace
  class Order < ServiceBase

    def self.index(status, options)
      user, current_store, params, request = prepare options

      if status.present? && ['cancelled','completed','executing'].include?(status)
        orders = user.orders.send("#{status}_orders")
      else
      	orders = user.orders
      end

      MarketPlace::Result::Success.new orders: orders
    end

    def self.show(id, options)
      user, current_store, params, request = prepare options

      order = ::Order.find(id)
      MarketPlace::Result::Success.new order: order
    end

    def self.create(order, options)
      user, current_store, params, request = prepare options
      if current_store.orders.where(user_id: user.id, status: 2).count >= current_store.max_pending_order
        raise MarketPlace::Errors::MaxPendingOrders
      end
      order.user_id = user.id
      order.delivery_price = user.cart.delivery_price
      if order.save
        order.add_items_from_cart(user.cart)
        ::Notifier.new_order_message(order).deliver
      else
        raise MarketPlace::Errors::UnprocessableEntity
      end

      MarketPlace::Result::Success.new order: order
    end
  end
end
