module MarketPlace
  module Api
    module Errors
      Unauthorized = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.unauthorized'))
      InvalidProductID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_profile_id'))
      PlanRestricted = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.plan_restricted'))
      InvalidTime = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_time'))
      InvalidEmailOrPassword = GraphQL::ExecutionError.new(I18n.t('devise.failure.invalid'))
      InvalidUserID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_user_id'))
      InvalidConversationID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_conversation_id'))

      InvalidEmail = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_email'))
      InvalidResetPasswordToken = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_reset_password_token'))

      InvalidProfileAdminID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_profile_admin_id'))

      InvalidAdminID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_admin_id'))

      InvalidAmount = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_amount'))
      InvalidReceiverEmail = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_receiver_email'))

      PayoutFailed = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.payout_failed'))

      InvalidPlanID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_plan_id'))

      InvalidProfileAdminEmail = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_profile_admin_email'))

      CouldntCreateFacebookPage = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.couldnt_create_facebook_page'))
      CouldntCreateFacebookGroup = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.couldnt_create_facebook_group'))  
      AcceptAgreement = GraphQL::ExecutionError.new(I18n.t 'errors.messages.graphql.accept_agreement')

      InvalidTicketID = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.invalid_ticket_id'))

      DuplicateProfileAdmin = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.duplicate_profile_admin'))

      DuplicateReport = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.duplicate_spam_report'))
      DuplicateBlock = GraphQL::ExecutionError.new(I18n.t('errors.messages.graphql.duplicate_advertiser_block'))
    end
  end
end
