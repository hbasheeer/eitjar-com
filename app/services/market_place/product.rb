module MarketPlace
  class Product < ServiceBase

    def self.index(category, options)
      user, current_store, params, request = prepare options

      if category
        if category.has_child?
          ids = category.subcategories.pluck(:id)
          products = current_store.products.active.where(category_id: ids)
        else
          products = current_store.products.active.where(category_id: category.id)
        end
      else
        products = current_store.products.active
      end

      MarketPlace::Result::Success.new products: products
    end

    def self.similar(product, options)
      user, current_store, params, request = prepare options

      ids = current_store.products.where(category_id: product.category_id).where('id != ?',product.id).pluck(:id).sample(4)
      products = ::Product.find ids

      MarketPlace::Result::Success.new products: products
    end

    def self.show(id, options)
      user, current_store, params, request = prepare options

      product = current_store.products.find(id)
      raise MarketPlace::Errors::NotFound if product.hidden?

      MarketPlace::Result::Success.new product: product
    end

  end
end
