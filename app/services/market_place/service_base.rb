module MarketPlace
  class ServiceBase
    class << self
      include Rails.application.routes.url_helpers
    end
    
    def self.prepare(options)
      return options[:current_user], options[:current_store] ,options[:params], options[:request] if !options.nil?
    end

  end
end
