module MarketPlace
  class Category < ServiceBase

    def self.index(options)
      user, current_store, params, request = prepare options

      categories = Rails.cache.fetch("categories", expires_in: 1.year) do
        current_store.categories
      end

      MarketPlace::Result::Success.new categories: categories
    end

  end
end
