module MarketPlace
  module Errors
    class Error < StandardError

      def initialize(object = nil)
        @object = object
      end

      def object
        @object
      end

    end
  end
end
