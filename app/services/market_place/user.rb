module MarketPlace
  class User < ServiceBase
    def self.show(id, options)
      user, params, request = prepare options
      raise MarketPlace::Errors::NotFound if id.nil?
      user = ::User.find(id)

      MarketPlace::Result::Success.new user: user
    end

    def self.update(attrs, options)
      user, params, request = prepare options
      raise MarketPlace::Errors::NotFound unless user
      user.update_attributes(attrs) 
      MarketPlace::Result::Success.new user: user
    end
  end
end
