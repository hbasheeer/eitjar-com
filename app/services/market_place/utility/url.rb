module MarketPlace
  module Utility
    module Url

      module_function

      # www.example.com => www.example.com
      # www.example.com:3000 => www.example.com
      def strip_port_from_host(host)
        host.split(":").first
      end
    end
  end
end