module MarketPlace
  module Utility
    module Store

      module_function

      def resolve_from_host(host)
        ident = ident_from_host(host, ::MarketPlace::Utility::Url.strip_port_from_host(::Rails.application.credentials[Rails.env.to_sym][:host]))
        if ident.present?
          ::Store.find_by(ident: ident)
        else
          ::Store.find_by(domain: host)
        end
      end

      def ident_from_host(host, app_domain)
        ident_with_www = /^www\.(.+)\.#{app_domain}$/.match(host)
        ident_without_www = /^(.+)\.#{app_domain}$/.match(host)

        if ident_with_www
          ident_with_www[1]
        elsif ident_without_www
          ident_without_www[1]
        end
      end
    end
  end
end