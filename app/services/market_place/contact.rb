module MarketPlace
  class Contact < ServiceBase

    def self.create(contact, options, flash)
      user, current_store, params, request = prepare options

      if contact.save
        # TODO: make sending email by background job
        ::Notifier.new_contact_message(@contact).deliver
        flash[:notice] =  "Thank you! Your message has been sent successfully."
      else
        raise MarketPlace::Errors::UnprocessableEntity
      end

      MarketPlace::Result::Success.new contact: contact
    end

  end
end
