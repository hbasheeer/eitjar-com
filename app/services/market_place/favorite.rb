module MarketPlace
  class Favorite < ServiceBase

    def self.index(options)
      user, current_store, params, request = prepare options

      favorites = current_store.favorites.where(user_id: user.id)

      MarketPlace::Result::Success.new favorites: favorites
    end

    def self.create(product, options)
      user, current_store, params, request = prepare options

      raise MarketPlace::Errors::NotFound unless product
      favorite = current_store.favorites.where(product_id: product.id, user_id: user.id).first

      if favorite
        newfavorite = favorite
      else
        newfavorite = current_store.favorites.create(product_id: product.id, user_id: user.id)
      end

      MarketPlace::Result::Success.new favorite: newfavorite
    end

    def self.destroy(product, options)
      user, current_store, params, request = prepare options

      raise MarketPlace::Result::NotFound unless product
      favorite = current_store.favorites.where(product_id: product.id, user_id: user.id).first
      favorite.destroy if favorite

      MarketPlace::Result::Success.new 
    end

  end
end
