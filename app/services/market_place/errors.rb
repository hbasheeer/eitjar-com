module MarketPlace
  module Errors
    class UnprocessableEntity < MarketPlace::Errors::Error; end
    class NotLogedIn < MarketPlace::Errors::Error; end
    class NoUser < MarketPlace::Errors::Error; end
    class NotFound < MarketPlace::Errors::Error; end
    class ImagesMissed < MarketPlace::Errors::Error; end
    class MaxPendingOrders < MarketPlace::Errors::Error; end
    class MaxUsersLimit < MarketPlace::Errors::Error; end
    class MaxProductsLimit < MarketPlace::Errors::Error; end
    class InvalidPaymentMethod < MarketPlace::Errors::Error; end
    class EmptyCoupon < MarketPlace::Errors::Error; end
    class InvalidCoupon < MarketPlace::Errors::Error; end
  end
end
