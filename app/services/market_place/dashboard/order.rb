module MarketPlace
  module Dashboard
    class Order < ServiceBase

      def self.index(status, options)
        user, store, params, request = prepare options
        orders_count = {}
        ::Order::POSSIBILE_STATUS.each {|status| orders_count[status] = store.orders.send("#{status}_orders").count}
        if status.present? && ::Order::POSSIBILE_STATUS.include?(status)
          orders = store.orders.send("#{status}_orders")
        else
          orders = store.orders
        end

        MarketPlace::Result::Success.new orders: orders, orders_count: orders_count
      end

      def self.update(order, order_params, options)
        user, store, params, request = prepare options

        raise MarketPlace::Errors::UnprocessableEntity unless order.update(order_params)

        MarketPlace::Result::Success.new order: order
      end
    end
  end
end
