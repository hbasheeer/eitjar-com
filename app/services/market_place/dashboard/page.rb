module MarketPlace
  module Dashboard
    class Page < ServiceBase

      def self.create(page, options)
        user, current_store, params, request = prepare options

        raise MarketPlace::Errors::UnprocessableEntity unless page.save

        MarketPlace::Result::Success.new page: page
      end

      def self.update(page, page_params, options)
        user, current_store, params, request = prepare options

        raise MarketPlace::Errors::UnprocessableEntity unless page.update(page_params)

        MarketPlace::Result::Success.new page: page
      end
    end
  end
end
