module MarketPlace
  module Dashboard
    class Subscribe < ServiceBase

      PAYMENT_METHODS = ['coupon']

      def self.upgrade(options, subscribe_param )
        user, current_store, params, request = prepare options

        payment_method = subscribe_param[:payment_method]
        raise MarketPlace::Errors::EmptyCoupon if subscribe_param[:coupon].blank?
        raise MarketPlace::Errors::InvalidPaymentMethod  unless PAYMENT_METHODS.include?(payment_method)

        case payment_method
        when "coupon"
          coupon = ::Coupon.find_by(code: subscribe_param[:coupon])
          if coupon && coupon.valid_code?
            handle_upgrade_by_coupon(coupon, current_store)
          else
            raise MarketPlace::Errors::InvalidCoupon
          end
        when "paypal"
          # handle paypal payment here
        when "creditcard"
          # handle creditcard payment here
        end

        MarketPlace::Result::Success.new
      end

      def self.handle_upgrade_by_coupon(coupon, current_store)
        plan_options = {
          plan_type: coupon.plan_type,
          features: ::StorePlan::PREMIUM_PLAN_FEATURES
          expires_at: Time.now + coupon.plan_duration.months
        }
        subscribe_log_options ={
          plan_type: coupon.plan_type,
          payment_method: "Coupon",
          duration: coupon.plan_duration,
          detail: "upgrade to #{coupon.plan_type} plan for #{coupon.plan_duration} months by Coupon's code #{coupon.code}",
        }
        coupon.update(used_by: current_store.id, used: true)
        current_store.plan.update(plan_options)
        current_store.subscribe_logs.create(subscribe_log_options)
      end
    end
  end
end
