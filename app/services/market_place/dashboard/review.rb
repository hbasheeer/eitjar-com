module MarketPlace
  module Dashboard
    class Review < ServiceBase

      def self.index(options)
        user, current_store, params, request = prepare options

        reviews = current_store.reviews.order(created_at: :desc)
        MarketPlace::Result::Success.new reviews: reviews
      end

      def self.update(review, review_params, options)
        user, current_store, params, request = prepare options

        if review.update(review_params)
        else
          raise MarketPlace::Errors::UnprocessableEntity
        end

        MarketPlace::Result::Success.new review: review
      end
    end
  end
end
