module MarketPlace
  module Dashboard
    class Main < ServiceBase

      def self.index(options)
        user, current_store, params, request = prepare options
        report_hash = {}
        report_hash[:users_count] = current_store.users.count
        report_hash[:new_orders_count] = current_store.orders.executing_orders.count
        report_hash[:cancelled_orders_count] = current_store.orders.cancelled_orders.count
        report_hash[:orders_count] = current_store.orders.count
        report_hash[:reviews_count] = current_store.reviews.count
        report_hash[:products_count] = current_store.products.count
        report_hash[:total_sales] = current_store.total_sales("today")

        MarketPlace::Result::Success.new report_hash: report_hash
      end

      def self.report(period, options)
        user, current_store, params, request = prepare options

        report_hash = {}
        report_hash[:cancelled_orders_count] = current_store.orders.cancelled_orders.send(period).count
        report_hash[:orders_count] = current_store.orders.send(period).count
        report_hash[:total_sales] = current_store.total_sales(period)

        MarketPlace::Result::Success.new report_hash: report_hash
      end
    end
  end
end
