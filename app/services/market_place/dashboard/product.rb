module MarketPlace
  module Dashboard
    class Product < ServiceBase

      def self.index(options)
        user, current_store, params, request = prepare options
        filter = params[:filter]
        if filter.present? && POSSIBALE_FILTER.include?(filter)
          products = current_store.products.send(filter).order(created_at: :desc)
        else
          products = current_store.products.order(created_at: :desc)
        end
        MarketPlace::Result::Success.new products: products
      end

      def self.create(product, options)
        user, current_store, params, request = prepare options
        raise MarketPlace::Errors::MaxProductsLimit unless current_store.can_add_product?
        if !product.category_id.present? && product.new_category_name.present?
          category = current_store.categories.create(name: product.new_category_name)
          product.category = category
        end
        if product.save
          current_store.update_setup_step("products")
        else
          raise MarketPlace::Errors::UnprocessableEntity
        end

        MarketPlace::Result::Success.new
      end

      def self.update(product, product_params, options)
        user, current_store, params, request = prepare options
        unless product.update(product_params)
          raise MarketPlace::Errors::UnprocessableEntity
        end

        MarketPlace::Result::Success.new product: product
      end
    end
  end
end
