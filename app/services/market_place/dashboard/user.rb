module MarketPlace
  module Dashboard
    class User < ServiceBase

      def self.index(options)
        user, current_store, params, request = prepare options

        users = current_store.users

        MarketPlace::Result::Success.new users: users
      end

      def self.update(order, cart, options)
        user, current_store, params, request = prepare options

        order.user_id = user.id
        order.delivery_price = cart.delivery_price
        order.add_cart_items_from_cart(cart)


        MarketPlace::Result::Success.new order: order
      end
    end
  end
end
