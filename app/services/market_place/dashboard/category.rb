module MarketPlace
  module Dashboard
    class Category < ServiceBase

      def self.index(options)
        user, current_store, params, request = prepare options

        categories = current_store.categories.order(created_at: :desc)
        MarketPlace::Result::Success.new categories: categories
      end

      def self.create(category, options, flash)
        user, current_store, params, request = prepare options

        if category.save
          flash[:notice] = 'تم إضافة التصنيف بنجاح.'
        else
          raise MarketPlace::Errors::UnprocessableEntity
        end

        MarketPlace::Result::Success.new
      end

      def self.update(category, category_params, options, flash)
        user, current_store, params, request = prepare options

        if category.update(category_params)
          flash[:notice] = 'تم تعديل التصنيف بنجاح.'
        else
          raise MarketPlace::Errors::UnprocessableEntity
        end

        MarketPlace::Result::Success.new category: category
      end
    end
  end
end
