xml.instruct!

xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc root_url
    xml.changefreq("hourly")
    xml.priority "1"
  end
  @products.each do |p|  
    xml.url do
      xml.loc product_url(category_slug: p.category.slug, id: p)
      xml.changefreq("daily")
      xml.priority "0.8"
      xml.lastmod p.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end

  @pages.each do |post|  
    xml.url do
      xml.loc '<%= Rails.application.credentials[Rails.env.to_sym][:host] %>' + post.slug
      xml.changefreq("daily")
      xml.priority "0.9"
      xml.lastmod post.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end
end