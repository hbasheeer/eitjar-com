module UsersHelper
  def print_user_status(status)
    if status
      "<span>#{t('user.print_status.banned')}</span>".html_safe
    else
      "<span>#{t('user.print_status.unbanned')}</span>".html_safe
    end
  end
end