module ProductsHelper
  def print_product_status(status)
    case status
    when 'active'
      "<span>#{t('product.print_status.active')}</span>".html_safe
    when 'hidden'
      "<span>#{t('product.print_status.hidden')}</span>".html_safe
    end
  end
end