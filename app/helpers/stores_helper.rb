module StoresHelper

  def print_delivery_start_at
    Time.parse("#{@current_store.delivery_start_at}:00").strftime("%l %P")
  end

  def print_delivery_end_at
    Time.parse("#{@current_store.delivery_end_at}:00").strftime("%l %P")
  end

  def print_store_products_limit
    if @current_plan.free?
      "<span dir='ltr'>(<span>#{@current_store.products.count}</span> / <span title='asdsad'> #{@current_plan.product_limit}</span>)</span>".html_safe
    end
  end
end
