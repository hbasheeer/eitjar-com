module PagesHelper
  def print_page_status(published)
    case published
    when true
      "<span>#{t('page.print_status.published')}</span>".html_safe
    when false
      "<span>#{t('page.print_status.unpublished')}</span>".html_safe
    end
  end
end