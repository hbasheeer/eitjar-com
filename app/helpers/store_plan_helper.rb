module StorePlanHelper
  def print_current_plan_type(type = @current_plan.plan_type)
    case type
    when "free"
      "الاساسي"
    when "premium"
      "المميز"
    end
  end

  def print_current_plan_expire_date
    if @current_plan.free?
      "غير محدد"
    elsif @current_plan.premium?
      @current_plan.expires_at.strftime("%F")
    end
  end

  def options_for_plan_types
    StorePlan.plan_types.keys.map { |w| [print_current_plan_type(w), w]  if  !w.eql?("free")}.compact
  end
end