module CategoriesHelper
  def print_category_status(status)
    if status
      "<i class='fa fa-eye-slash' title='مخفي'></i>".html_safe
    else
      "<i class='fa fa-eye'></i>".html_safe
    end
  end
end
