module ReviewsHelper
  def print_review_status(status)
    if status
      "<i class='fa fa-eye-slash' title='مخفي'></i>".html_safe
    else
      "<i class='fa fa-eye'></i>".html_safe
    end
  end 
end