module ApplicationHelper
  include Pagy::Frontend

  def gravatar_for(user, size = 30, title = user.name, classname = "userImg")
    image_tag gravatar_image_url(user.email, size: size), title: title, class: classname
  end
  
  def title(text)
    content_for :title,text
  end

  def meta_tag(tag, text)
    content_for :"meta_#{tag}", text
  end

  def yield_meta_tag(tag, default_text='')
    content_for?(:"meta_#{tag}") ? content_for(:"meta_#{tag}") : default_text
  end

  def delivary_time
    start_at = Time.zone.now.beginning_of_day + @current_store.delivery_start_at.to_i.hour

    if Time.zone.now.hour > @current_store.delivery_end_at.to_i
      delivary_start = start_at + 1.day
    else
      delivary_start = start_at
    end
    
    return delivary_start.strftime('%B %d, %Y  %H:%M:%S')
  end

  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end

end
