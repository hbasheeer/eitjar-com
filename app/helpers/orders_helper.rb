module OrdersHelper
  def print_order_status(status)
 	  case status
 	  when 'executing'
 	    t('order.print_status.executing')
 	  when 'completed'
 	    t('order.print_status.completed')
 	  when 'cancelled'
 	    t('order.print_status.cancelled')
 	  end
  end

  def print_orders_filter(status)
    case status
    when 'executing'
      "جارية التنفيذ"
    when 'completed'
      "المكتملة"
    when 'cancelled'
      "الملغاة"
    else
      "جميع الطلبات"
    end
  end

  def order_label_class(status)
    case status
    when 'executing'
      "primary"
    when 'completed'
      "success"
    when 'cancelled'
      "danger"
    else
      "جميع الطلبات"
    end
  end

end