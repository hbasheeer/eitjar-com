# == Schema Information
#
# Table name: pages
#
#  id                     :integer          not null, primary key
#  title                  :string       
#  slug                   :string
#  content                :text
#  meta_keywords          :string
#  meta_description       :text
#  published              :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :pages, [:store_id, :slug]    unique: true


class Page < ApplicationRecord
  validates_presence_of :title, :slug, :content, :meta_keywords
  validates_format_of :slug, :with => /\A[A-Za-z]+([_]?[A-Za-z0-9]+)*\z/, :message => "رابط الصفحة غير صحيح. ادخل أحرف إنجليزية و أرقام و _ فقط"
  validates_length_of :slug, minimum: 3

  scope :published, -> {where(published: true)}
  belongs_to :store, counter_cache: true

  def url
  	store.full_url + "/p/#{slug}"
  end
end
