# == Schema Information
#
# Table name: coupons
#
#  id                     :integer          not null, primary key
#  code                   :string
#  used_by                :integer       
#  used                   :boolean
#  plan_type              :string
#  plan_duration          :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :coupons, :used_by
#  add_index :coupons, :code

class Coupon < ApplicationRecord
  belongs_to :owner, optional: true, class_name: "Store", foreign_key: 'used_by'

  def valid_code?
    used_by.nil? && !used
  end
end
