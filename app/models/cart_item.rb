# == Schema Information
#
# Table name: cart_items
#
#  id                     :integer          not null, primary key
#  product_id             :integer
#  store_id               :integer
#  quantity               :integer
#  price                  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :cart_items, :cart_id
#  add_index :cart_items, :store_id
#  add_index :cart_items, :product_id

class CartItem < ApplicationRecord
  belongs_to :store, optional: true
  belongs_to :product
  belongs_to :cart

  def total_price
	  price * quantity
  end

  def to_hash
    hash = {}
    self.attributes.each { |k,v| hash[k] = v }
    return hash
  end

  def can_increase_quantity?
    return true if product.is_infinite?
    product.in_stack - quantity > 0
  end

end
