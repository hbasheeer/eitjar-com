# == Schema Information
#
# Table name: products
#
#  id                     :integer          not null, primary key
#  title                  :string       
#  description            :text
#  category_id            :integer
#  store_id               :integer
#  price                  :integer
#  in_stack               :integer
#  is_infinite            :boolean
#  featured               :boolean
#  status                 :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :products, :store_id
#  add_index :products, :category_id

# unit_type
class Product < ApplicationRecord
  attr_accessor :new_category_name

  PRODUCT_STATUS = {
    active: 0,
    hidden: 1
  }.freeze

  enum status: PRODUCT_STATUS

  POSSIBALE_FILTER = []
  has_many_attached :images

  validates :images, attached: true, 
                content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                dimension: { width: { min: 800, max: 2400 }, height: { min: 600, max: 1800 }},
                limit: { min: 1, max: 10 },
                size: { less_than: 2.megabytes }

  after_initialize :set_default_status, :if => :new_record?
  before_destroy :ensure_not_referenced_by_any_cart_item
  
  validates_presence_of :title, :price
  validates_presence_of :category_id, if: lambda { new_category_name.blank? }

  belongs_to :category, optional: true
  belongs_to :store, counter_cache: true
  has_many :items, class_name: "CartItem"
  has_many :orders, through: :items  
  has_many :favorites
  has_many :reviews
  has_one  :product_offer

  scope :featured, ->  { where( featured: true) }

  def to_param
    [id, title].join("-")
  end

  def as_json(options={})
    attrs = [:id, :title, :description, :price ]
    super(only: attrs, methods: [:image_url], :include => { :category => { :only => [:ar_name, :slug] }})
  end

  def image_url
    image.url
  end

  def has_offer?
    product_offer.present? && product_offer.end_at > Time.now && product_offer.active?
  end 

  def url
    "/products/#{category.slug}/#{to_param}"
  end

  def get_price 
    has_offer? ? offer_price : price
  end

  def offer_price
    product_offer.price
  end

  def offer_end_at
    product_offer.end_at
  end

  def average_rate
    return 0 if reviews.active.count == 0
    reviews.active.sum(:rate) / reviews.active.count
  end

  def available?
    is_infinite? || in_stack > 0
  end
 
  private 

  # ensure that there are no cart items referencing this product
  def ensure_not_referenced_by_any_cart_item
    if items.empty?
      return true
    else
      errors.add(:base, 'Cart Items present')
      return false
    end
  end 

  def set_default_status
    self.status ||= :active
  end

end
