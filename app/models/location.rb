# == Schema Information
#
# Table name: locations
#
#  id                     :integer      not null, primary key
#  location_type          :string
#  latitude               :float
#  longitude              :float
#  google_address         :string
#  address                :string
#  fullname               :string
#  mobile                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :locations, :store_id
#  add_index :locations, :user_id

class Location < ApplicationRecord
  belongs_to :user
  belongs_to :store
  has_many :orders

  validates_presence_of :location_type, :address, :fullname, :mobile
  
  def has_map?
    latitude && longitude
  end
end
