# == Schema Information
#
# Table name: product_offers
#
#  id                     :integer          not null, primary key
#  product_id             :integer
#  start_at               :datetime
#  end_at                 :datetime
#  active                 :boolean          default: true
#  price                  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null

class ProductOffer < ApplicationRecord
  belongs_to :product

  validates_uniqueness_of :product_id
  validates :price, numericality: { only_integer: true }
  validates_presence_of :start_at, :end_at
  
  def active?
    active 
  end
end
