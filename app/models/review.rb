# == Schema Information
#
# Table name: reviews
#
#  id                     :integer          not null, primary key
#  product_id             :integer
#  user_id                :integer
#  store_id               :integer
#  content                :text
#  rate                   :integer
#  deleted                :boolean
#  hidden                 :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :reviews, :user_id
#  add_index :reviews, :store_id
#  add_index :reviews, :product_id
#

class Review < ApplicationRecord
	belongs_to :user, optional: true
	belongs_to :product, optional: true
  belongs_to :store

	validates_presence_of :rate, :content

	scope :active, -> {where.not(hidden: true)}
end
