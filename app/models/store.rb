# == Schema Information
#
# Table name: stores
#
#  id                     :integer          not null, primary key
#  ident                  :string
#  slogan                 :string
#  description            :text
#  domain                 :string
#  use_domain             :boolean
#  google_maps_key        :string
#  google_analytics_key   :string
#  logo                   :attached
#  cover_photo            :attached
#  favicon                :attached
#  meta_title             :string
#  meta_description       :text
#  facebook               :string
#  twitter                :string
#  instagram              :string
#  enable_google_maps     :boolean
#  currency               :string
#  min_order_total        :integer
#  enable_reviews         :boolean
#  max_pending_order      :integer
#  delivery_amount        :integer
#  free_shipping_amount   :integer
#  users_count            :integer
#  products_count         :integer
#  orders_count           :integer
#  reviews_count          :integer
#  categories_count       :integer
#  pages_count            :integer
#  email                  :string
#  address                :string
#  mobile                 :string
#  delivery_start_at      :datetime
#  delivery_end_at        :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :stores, :ident,  unique: true
#  add_index :stores, :domain, unique: true

class Store < ApplicationRecord
  has_one_attached :cover_photo
  has_one_attached :logo
  has_one_attached :favicon

  validates :cover_photo, content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                dimension: { width: { min: 1200, max: 2400 }, height: { min: 400, max: 800 }},
                limit: { min: 1, max: 10 },
                size: { less_than: 2.megabytes }

  validates :logo, content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                dimension: { width: { min: 256, max: 512 }, height: { min: 256, max: 512 }},
                limit: { min: 1, max: 10 },
                size: { less_than: 512.kilobytes }

  validates :favicon, content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                dimension: { width: { min: 32, max: 64 }, height: { min: 32, max: 64 }},
                limit: { min: 1, max: 10 },
                size: { less_than: 256.kilobytes }

  #validates :ident, presence: true, allow_blank: false
  validates_uniqueness_of :ident
  validates_format_of :ident, :with => /\A[A-Za-z]+([_]?[A-Za-z0-9]+)*\z/, :message => "اسم المتجر غير صحيح. ادخل أحرف إنجليزية و أرقام و _ فقط"

  has_many :users
  has_many :orders
  has_many :order_items
  has_many :reviews
  has_many :products
  has_many :contacts
  has_many :categories
  has_many :favorites
  has_many :pages
  has_many :carts
  has_many :cart_items
  has_many :locations

  has_one :setup_step
  has_one :plan, class_name: "StorePlan"
  has_many :subscribe_logs, class_name: "StoreSubscribeLog"
  
  belongs_to :owner, optional: true, class_name: "User", foreign_key: 'owner_id'

  # Returns the full domain with default protocol in front
  def full_url(options= {})
    if Rails.env.development?
      full_domain(:with_protocol => true, port: 3000)
    else
      full_domain(:with_protocol => true)
    end
  end

  def dashboard_url
    "#{full_url}/dashboard"
  end

  #returns full domain without protocol
  def full_domain(options= {})
    # assume that if port is used in domain config, it should
    # be added to the end of the full domain for links to work
    # This concerns usually mostly testing and development
    default_host, default_port = Rails.application.credentials[Rails.env.to_sym][:host].split(':')
    port_string = options[:port] || default_port

    if domain.present? && use_domain? # custom domain
      dom = domain
      dom += ":#{port_string}" unless port_string.blank?
    else # just a subdomain specified
      dom = "#{self.ident}.#{default_host}"
      dom += ":#{port_string}" unless port_string.blank?
    end

    if options[:with_protocol]
      dom = "#{(Settings.always_use_ssl.to_s == "true" ? "https://" : "http://")}#{dom}"
    end

    return dom
  end

  def update_setup_step(step)
    case step
    when "details"
      result = slogan.present? && description.present?
      setup_step.update_attribute(:slogan_and_description, result)
    when "design"
      result = cover_photo.present? && favicon.present? && logo.present?
      setup_step.update_attribute(:visual_identity, result)
    when "products"
      result = products.present?
      setup_step.update_attribute(:products, result)
    when "configuration"
      result = %i(currency min_order_total max_pending_order free_shipping_amount
       delivery_start_at delivery_end_at).all? { |key| key.present? }
      setup_step.update_attribute(:configuration, result)
    end
  end

  def enabled_maps?
    enable_google_maps? && !google_maps_key.blank?
  end

  def total_sales(period)
    case period
    when "today"
      orders.completed_orders.today.sum {|order| order.total_price}
    when "month"
      orders.completed_orders.today.sum {|order| order.total_price}
    when "all"
      orders.completed_orders.sum {|order| order.total_price}
    end
  end

  def delivery_time?
    delivery_time_range.include?(Time.zone.now.hour)
  end

  def delivery_time_range
    delivery_start_at.to_i..delivery_end_at.to_i
  end

  def can_add_product?
    products_count < plan.get_features("products_limit")
  end

  def can_add_user?
    users_count < plan.get_features("users_limit")
  end

  def can_use_google_map?
    plan.get_features("google_map")
  end

  def can_custom_domain?
    plan.get_features("custom_domain")
  end

  def from_mail
    if slogan.present? && email.present?
      "#{slogan} <#{email}>"
    else
      full_domain
    end
  end

  def reply_to_mail
    email.present? ? email : "no_reply@#{full_domain}"
  end
end
