# == Schema Information
#
# Table name: order_items
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  store_id               :integer
#  product_id             :integer
#  order_id               :integer
#  quantity               :integer
#  price                  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :order_items, :order_id
#  add_index :order_items, :cart_id
#  add_index :order_items, :store_id
#  add_index :order_items, :product_id

class OrderItem < ApplicationRecord
  belongs_to :store, optional: true
  belongs_to :product
  belongs_to :cart
  belongs_to :order

  def total_price
    price * quantity
  end
end
