# == Schema Information
#
# Table name: carts
#
#  id                     :integer      not null, primary key
#  user_id                :integer
#  store_id               :integer
#  unique_id              :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :carts, :unique_id,   unique: true
#  add_index :carts, :user_id, unique: true
#  add_index :carts, :store_id, unique: true

class Cart < ApplicationRecord
  include Cartable

  belongs_to :user, optional: true
  belongs_to :store, optional: true
  has_many :items, class_name: 'CartItem', dependent: :destroy

  after_initialize :generate_uniqe_id, :if => :new_record?

  def generate_uniqe_id
    self.unique_id = SecureRandom.hex
  end

  def find_product?(product_id)
    items.find_by(product_id: product_id).present? ? true : false
  end

  def price
    items.to_a.sum { |item| item.total_price }
  end

  def total_price
    price + delivery_price
  end

  def total_item
    items.to_a.sum {|item| item.quantity}
  end

  def empty?
    items.count == 0
  end

  def delivery_price
    free_delivery_amount = store.free_delivery_with_amount
    delivery_price = 0
    if price != 0 && (price < free_delivery_amount) 
      delivery_price = store.delivery_amount
    end
    delivery_price
  end

  def free_delivery?
    (Settings.free_delivery.start_at..Settings.free_delivery.end_at).include? Time.zone.now.hour
  end

  def total_less_than_min
    price < store.min_order_total
  end

end
