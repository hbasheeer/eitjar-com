# == Schema Information
#
# Table name: users
#
#  id                     :integer      not null, primary key
#  fullname               :string
#  username               :string
#  country                :string
#  mobile                 :integer
#  address                :string
#  role                   :integer
#  store_id               :integer
#  email                  :string       null: false, default: ""
#  encrypted_password     :string
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer   
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string     # Only if using reconfirmable
#  provider               :string
#  uid                    :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :users, :email,                unique: true
#  add_index :users, :reset_password_token, unique: true
#  add_index :users, :confirmation_token,   unique: true
#  add_index :users, [:store_id, :email]    unique: true

class User < ApplicationRecord
  include Authable

  attr_accessor :terms, :ident
  enum role: [:user, :admin]

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :confirmable, :lockable,
         :validatable, :omniauthable, omniauth_providers: [:facebook]

  self._validators[:email].reject!{|v| v.class == ActiveRecord::Validations::UniquenessValidator}
  self._validate_callbacks.delete self._validate_callbacks.find{|v| v.filter.class == ActiveRecord::Validations::UniquenessValidator && v.filter.attributes == [:email]}
  validates_uniqueness_of :email, allow_blank: true, if: :will_save_change_to_email?, scope: :store_id
  validates_presence_of :fullname
  validates :terms, :acceptance => true
  after_initialize :set_default_role, :if => :new_record?
  
  has_many :orders
  has_many :identities
  has_many :favorites
  has_many :reviews
  has_many :locations
  has_one  :cart

  belongs_to :store, optional: true, counter_cache: true
  accepts_nested_attributes_for :store

  
  def set_default_role
    self.role ||= :user
  end
  
  def token
    #email_confirmed ? json_web_token : nil
    json_web_token
  end

  def json_web_token
    ::JsonWebToken.encode(user: { id: id })
  end

  def email_confirmed
    !confirmed_at.nil?
  end

  def as_json(options={})
    attrs = [:id, :email, :fullname, :mobile]
    super(only: attrs, methods: [:token])
  end

  def favorite_product?(product_id)
    favorites.where(product_id: product_id).first
  end

  def review_product?(product_id)
    reviews.where(product_id: product_id).first
  end

  def can_add_review?(product_id)
    !reviews.map(&:product_id).include?(product_id)
  end

  def can_add_order?
    orders.completed_orders.count < store.max_pending_order
  end

  def banned?
    locked_at.present?
  end

  #def self.find_for_authentication(warden_conditions)
  #  store = Store.find_by(ident: warden_conditions[:ident])
  #  where(:email => warden_conditions[:email], :store_id => store.try(:id) ).first
  #end  
end
