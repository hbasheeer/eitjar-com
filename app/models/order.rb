# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  location_id            :integer    
#  order_cart_id          :integer
#  latitude               :float
#  longitude              :float
#  status                 :integer
#  payment_method         :string
#  detail                 :text
#  admin_detail           :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null

#  Indexes
#  add_index :orders, :user_id
#  add_index :orders, :store_id
#  add_index :orders, :location_id

class Order < ApplicationRecord
  POSSIBILE_STATUS = ['cancelled', 'completed', 'executing']
  PAYMENT_METHODS = ['cash_on_delivery', 'paypal', 'credit_card']

  after_initialize :set_default_status, :if => :new_record?

  has_many :items, class_name: 'OrderItem', dependent: :destroy
  belongs_to :user, optional: true
  belongs_to :store, counter_cache: true
  belongs_to :location
  
  enum status: [:executing, :completed, :cancelled]
  scope :executing_orders,  -> {where(status: 0)}
  scope :completed_orders, -> {where(status: 1)}
  scope :cancelled_orders, -> {where(status: 2)}


  scope :today, ->{where(:created_at => (Time.zone.now.beginning_of_day..Time.zone.now))}
  scope :week,->{where(:created_at => (Time.zone.now.beginning_of_week..Time.zone.now))}
  scope :month,->{where(:created_at => (Time.zone.now.beginning_of_month..Time.zone.now))}
  scope :yesterday, ->{where(:created_at => (Time.zone.now.beginning_of_day-1.day..Time.zone.now.beginning_of_day))}
  scope :prev_month,->{where(:created_at =>((Time.now.beginning_of_month-1.day)..Time.zone.now.beginning_of_month))}
  scope :three_month,->{where(:created_at =>((Time.zone.now-3.month).beginning_of_month..Time.zone.now.beginning_of_month))}


  def price
    items.sum {|item| item.price * item.quantity} 
  end

  def total_price
    price + delivery_price
  end

  def total_item
    items.sum[:quantity]
  end

  def empty?
    items.count == 0 
  end

  def delivery_price
    return 0 if price == 0 ||  price >= 25000
    return 3000 if price < 25000
  end

  def as_json(options={})
    attrs = [:id, :details]
    super(only: attrs, include: { 
      items: { only: [:quantity], include: { product: { only: [:id, :name, :description, :price ],  methods: [:image_url] } } } 
    })
  end 

  def add_items_from_cart(cart)
    cart.items.each do |item|
      unless item.product.is_infinite?
        item.product.in_stack -= item.quantity > item.product.in_stack ?  item.product.in_stack :  item.quantity
        item.product.save
      end 
      items.create!(
        store_id: item.store_id,
        product_id: item.product_id,
        order_id: item.order_id,
        cart_id: cart.id,
        quantity: item.quantity,
        price: item.price
      )
      item.destroy
    end 
  end

  def cancel_order
    return if executing? && (Time.now < created_at + 5.minute)
    cancelled!
    items.each do |item|
      item.product.in_stack += item.quantity
      item.product.save
    end 
  end

  def set_default_status
    self.status ||= :executing
  end

  def change_order_status_email
    ::Notifier.send("#{status}_order_message", order).deliver_later!
  end

  def telegram_message
    message = []
    message << "طلب جديد قد تم إضافته: ##{id}"
    message << "الرابط على الموقع: #{Settings.host}/admin/orders/#{id}"
    message << "التقاصيل:"

    items.each do |item|
      message << "#{items.index(item) + 1}- #{item.product.ar_name} ( X #{item.quantity})"
    end
    message << "سعر التوصيل: #{delivery_price} دينار"
    message << "سعر الطلب كاملا: #{total_price} دينار"

    message << "موقع الزبون: https://maps.google.com?q=#{lat},#{lng}"
    
    return message.join("\n")
  end
end
