# == Schema Information
#
# Table name: SetupSteps
#
#  id                     :integer      not null, primary key
#  store_id               :integer      
#  products               :boolean      default: false     
#  slogan_and_description :boolean      default: false
#  visual_identity        :boolean      default: false
#  configuration          :boolean      default: false
#  created_at             :datetime     not null
#  updated_at             :datetime     not null

#  Indexes
#  add_index :setup_steps, :store_id

class SetupStep < ApplicationRecord
  belongs_to :store

  def finished?
    products && slogan_and_description && visual_identity && configuration
  end
end
