module Cartable

  def add_product(product, store_id, quantity = 1)
    if current_item = items.find_by(product_id: product.id)
      if current_item.product.is_infinite?
        current_item.quantity += quantity
      else
        quantity = quantity > current_item.product.in_stack ?  current_item.product.in_stack :  quantity
        if (current_item.product.in_stack - current_item.quantity) >= 0 
          current_item.quantity += quantity
        end
      end
    else
      current_item = items.build(product_id: product.id, price: product.get_price, 
                                 quantity: quantity, store_id: store_id)
    end
    current_item
  end

  def remove_product(product_id)
    current_item = items.find_by(product_id: product_id)
    if current_item.quantity > 1
      current_item.quantity -= 1
    elsif current_item.quantity = 1
      current_item.destroy
    end
    current_item
  end
end