# Shared methods between models
module Sluggable
  extend ActiveSupport::Concern

  included do
    class_attribute :slugger

    before_validation :set_slug
  end

  def set_slug
    return if slugger.blank?

    if new_record?
      str = send(slugger)
    else # for update use existing slug not slugger
      str = send(:slug)
    end

    return if str.blank?

    self.slug = self.class.get_slug(str)
  end

  module ClassMethods
    def slug(str)
      self.slugger = str
    end

    def get_slug(str)
      str = str.strip.gsub(/\s+|\.+/, '-')
      str = str.scan(/[a-z]|[0-9]|[آ-ي]|ء|\-|\_/i)
      str = str.join.downcase
      str = str.gsub(/(\_)+/, '_')
      str = str.gsub(/(\-)+/, '-')
      str = str.gsub(/(\-(\_|\-)\-)+/, '-')
      str = str[0..-2] if /\-|\_|\./.match(str[-1].chr) unless str.blank?
      str = str[1..-1] if /\-|\_|\./.match(str[0].chr) unless str.blank?
      str
    end
  end
end
