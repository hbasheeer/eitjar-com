module Authable
  extend ActiveSupport::Concern

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    TEMP_EMAIL_PREFIX = 'change@me'

    def find_for_oauth(auth, store_id)
      puts auth
      # Get the identity and user if they exist
      identity = Identity.get_by_auth(auth, store_id)
      user = identity.user

      if user.nil?
        email = auth.info.email
        user = User.where(:email => email).first if email

        # Create the user if it's a new registration
        if user.nil?
          puts auth
          user = User.new(
            fullname: "#{auth.info.first_name} #{auth.info.last_name}",
            email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
            password: Devise.friendly_token[0,20],
            store_id: store_id
          )
          user.skip_confirmation!
          user.save!
        end
      end

      # Associate the identity with the user if needed
      if identity.user != user
        identity.user = user
        identity.save!
      end
      user
    end



  end

end