# == Schema Information
#
# Table name: favorites
#
#  id                     :integer          not null, primary key
#  user_id                :integer       
#  product_id             :integer
#  store_id               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :favorites, :user_id
#  add_index :favorites, :store_id
#  add_index :favorites, :product_id

class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :product
  belongs_to :store
end
