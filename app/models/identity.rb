# == Schema Information
#
# Table name: identities
#
#  id                     :integer      not null, primary key
#  provider               :string
#  uid                    :string
#  user_id                :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :identities, :uid,         unique: true
#  add_index :identities, :user_id

class Identity < ApplicationRecord
  belongs_to :user
  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider

  scope :facebook, ->() { where(provider: 'facebook') }
  scope :twitter, ->() { where(provider: 'twitter') }
  scope :google, ->() { where(provider: 'google_oauth2') }

  def self.get_by_auth(auth, store_id)
    find_or_create_by(uid: auth.uid, provider: auth.provider, store_id: store_id)
  end
end
