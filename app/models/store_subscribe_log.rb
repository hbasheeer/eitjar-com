# == Schema Information
#
# Table name: store_subscribe_logs
#
#  id                     :integer      not null, primary key
#  store_id               :integer
#  payment_method         :string
#  plan_type              :integer
#  duration               :integer
#  amount                 :integer
#  deatil                 :text
#  expires_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :users, :store_id,    unique: true

class StoreSubscribeLog < ApplicationRecord
  belongs_to :store
end
