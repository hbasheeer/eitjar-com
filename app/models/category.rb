# == Schema Information
#
# Table name: categories
#
#  id                     :integer          not null, primary key
#  name                   :string
#  slug                   :string
#  hidden                 :boolean
#  image                  :string           # attachment
#  parent_id              :integer       
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :categories, :store_id

class Category < ApplicationRecord
  include Sluggable

  slug :name
  has_many :products
  belongs_to :store, counter_cache: true
  
  validates :name, :slug, presence: true, allow_blank: false
  
  has_many :subcategories, :class_name => "Category", :foreign_key => "parent_id", :dependent => :destroy
  belongs_to :parent_category, :class_name => "Category", :foreign_key => "parent_id", optional: true

  scope :main, ->  { where( parent_id: nil) }

  def as_json(options={})
    attrs = [:id, :name, :slug]
    super(only: attrs, methods: [:image_url])
  end 

  def has_child?
    subcategories.present?
  end

  def parent?
    parent_category.present?
  end

  def image_url
  	image.present? ? image.url : nil
  end 
end
