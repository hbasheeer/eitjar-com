# == Schema Information
#
# Table name: store_plans
#
#  id                     :integer      not null, primary key
#  features               :jsonb
#  status                 :integer
#  plan_type              :integer
#  store_id               :integer
#  user_limit             :integer
#  product_limit          :integer
#  expires_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
#  Indexes
#  add_index :users, :store_id,    unique: true

class StorePlan < ApplicationRecord
  PLAN_STATUS = {
    active: 0,
    inactive: 1
  }.freeze
  
  PLAN_TYPE = {
    free: 0,
    premium: 1
  }.freeze

  FREE_PLAN_FEATURES = {
    users_limit: 300,
    products_limit: 25,
    custom_domain: false,
    google_map: false,
  }

  PREMIUM_PLAN_FEATURES = {
    users_limit: 3000,
    products_limit: 1000,
    custom_domain: true,
    google_map: true,
  }

  enum status: PLAN_STATUS  
  enum plan_type: PLAN_TYPE

  after_initialize :set_default_values, :if => :new_record?
  belongs_to :store

  def set_default_values
    self.status ||= :active
    self.plan_type ||= :free
  end

  def get_features(key)
    features[key] || eval("#{plan_type.upcase}_PLAN_FEATURES[:#{key}]")
  end

end
