# == Schema Information
#
# Table name: contacts
#
#  id                     :integer      not null, primary key
#  name                   :string
#  email                  :string
#  message                :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null

class Contact < ApplicationRecord
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i 
  validates_presence_of :message, :name

  belongs_to :store
end
