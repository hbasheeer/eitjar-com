require 'telegram/bot'

class Api::TelegramBotController < Api::BaseApiController
  skip_before_action :authenticate_user_from_token!

  def webhook
    Telegram::Bot::Client.run("Settings.telegram.token") do |bot|
      case params[:message][:text].split.first
      when '/start'
        user = TelegramUser.find_by(chat_id: params[:message][:chat][:id] )
        return if user.present?
      	user = TelegramUser.new
      	user.chat_id = params[:message][:chat][:id]
      	user.first_name = params[:message][:chat][:first_name]
      	user.last_name = params[:message][:chat][:last_name]
      	user.username = params[:message][:chat][:username]
      	user.admin = true
      	if user.save
          bot.api.send_message(chat_id: params[:message][:from][:id], text: "مرحبا #{user.first_name} تمت عملية الاشتراك بنجاح")
      	end
      when '/stop'
      	user = TelegramUser.find_by(chat_id: params[:message][:chat][:id] )
      	if user
      	  user.destroy
      	  bot.api.send_message(chat_id: params[:message][:from][:id], text: "مرحبا, #{params[:message][:from][:first_name]} تم إلغاء اشتراكك")
      	end	
        	
      else
        bot.api.send_message(chat_id: params[:message][:from][:id], text: "ما فهمت ؟ شلون فيني اخدمك")
      end
    end
  rescue
    nil
  end

end
