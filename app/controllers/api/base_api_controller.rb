class Api::BaseApiController < ActionController::API
  include Authenticatable
  include CurrentCart
  include SharedMethods

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user_from_token!,
                :current_store,
                :current_plan,
                :prepare_options,
                :set_cart_from_api

   
  def current_store
    @current_store = ::Store.first
    request.env[:store_id] = @current_store.try(:id) || nil
  end
end
