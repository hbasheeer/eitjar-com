class PageController < ApplicationController

  def show
    @page = Page.find_by(slug: params[:slug], store_id: @current_store.try(:id))
    render_not_found unless @page && @page.published?
  end
end
