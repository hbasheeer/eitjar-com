class CartController < StoreBaseController
  
  def index
    @contact = Contact.new
    @order = Order.new
  end
  
  # Destroy Cart
  def destroy
  	@cart.destroy if @cart.id == session[:cart_id]
  	session[:cart_id] = nil
  	respond_to do |format|
  		format.html { redirect_to cart_url }
  		format.json { head :no_content }
  	end
  end

end
