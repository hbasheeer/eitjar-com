class FavoritesController < StoreBaseController
	before_action :authenticate_user!
  before_action :set_product, only: [:create, :destroy]

  def index
    @pagy, @favorites = pagy(MarketPlace::Favorite.index(@options)[:favorites])
  end

  def create
    @favorite = MarketPlace::Favorite.create(@product, @options)[:favorite]
  rescue MarketPlace::Errors::NotFound
    render_not_found
  end

  def destroy
    @favorite = MarketPlace::Favorite.destroy(@product, @options)
    respond_to do |format|
      format.html { redirect_to favorites_path}
      format.js 
    end
  rescue MarketPlace::Errors::NotFound
    render_not_found
  end

  private

  def set_product
    @product = Product.find(params[:product_id])
  end
end
