class HomeController < ApplicationController
  skip_before_action :set_cart

  def landing_page
    @disable_nav = true
  end

end
