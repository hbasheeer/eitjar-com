class Dashboard::OrdersController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_order, except: [:new, :index]
  respond_to :html

  def index 
    result = MarketPlace::Dashboard::Order.index(params[:status], @options)
    puts result[:orders_count]
    @orders_count = result[:orders_count]
    @pagy, @orders = pagy(result[:orders])
  end

  def edit
  end

  def update
    MarketPlace::Dashboard::Order.update(@order, order_params, @options)
    redirect_back(fallback_location: dashboard_orders_url)
    flash[:notice] = 'تم تعديل الطلب بنجاح'
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = @current_store.orders.find(params[:id])
  end

  def order_params
    params.require(:order).permit(:admin_detail, :status)
  end

end