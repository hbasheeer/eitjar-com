class Dashboard::ProductsController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_product, except: [:create, :new, :index]
  respond_to :html

  def index
    @pagy, @products = pagy(MarketPlace::Dashboard::Product.index(@options)[:products])
  end

  def new
    if @current_store.can_add_product?
      @product = @current_store.products.new
    else
      redirect_to dashboard_products_url
      flash[:alert] = 'لقد تجاوزت الحد المسموح به لإضافة منتجات جديدة'
    end
  end

  def edit
  end

  def create
    @product = @current_store.products.new(product_params)
    MarketPlace::Dashboard::Product.create(@product, @options)
    flash[:notice] = 'تم إضافة المنتج بنجاح.'
    redirect_to dashboard_products_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :new
  rescue MarketPlace::Errors::MaxProductsLimit
    flash[:alert] = 'لقد تجاوزت الحد المسموح به لإضافة منتجات ضمن الحساب المجاني'
    redirect_to dashboard_products_url
  end

  def update
    MarketPlace::Dashboard::Product.update(@product, product_params, @options)
    flash[:notice] = 'تم إضافة المنتج بنجاح.'
    redirect_to dashboard_products_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = @current_store.products.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:title, :description, :category_id, :price, :status, :in_stack, :is_infinite, :new_category_name, :featured, images: [])
  end

end