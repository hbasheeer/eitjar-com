class Dashboard::ReviewsController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_review, only: [:edit, :hide, :unhide, :update]
  respond_to :html

  def index
    @pagy, @reviews = pagy(MarketPlace::Dashboard::Review.index(@options)[:reviews])
  end

  def edit
  end

  def update
    MarketPlace::Dashboard::Review.update(@review, review_params, @options)
    flash[:notice] = 'تم تعديل التقييم بنجاح.'
    redirect_to dashboard_reviews_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def hide
    @review.update(hidden: true)
  end

  def unhide
    @review.update(hidden: false)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_review
    @review = @current_store.reviews.find(params[:id])
  end

  def review_params
    params.require(:review).permit(:id, :hidden, :content)
  end
end