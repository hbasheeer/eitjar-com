class Dashboard::UsersController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_user, except: [:new, :index]
  respond_to :html

  def index
    @pagy, @users = pagy(MarketPlace::Dashboard::User.index(@options)[:users])
  end

  def edit
  end

  def update
    MarketPlace::Dashboard::User.update(@user, user_params, @options, flash)
    redirect_to dashboard_users_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def update_me
    MarketPlace::Dashboard::User.update(current_user, user_params, @options, flash)
    redirect_to dashboard_users_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def me
  end

  def ban
    @user.lock_access!(send_instructions: false) unless @user == @current_store.owner
    respond_to :js
  end

  def unban
    @user.unlock_access!
    respond_to :js
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = @current_store.users.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:id, :fullname, :email ,:mobile, :role)
    end
end