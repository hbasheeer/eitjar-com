class Dashboard::SetupStoresController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_variable
  respond_to :html

  def show 
  end

  def social_media
  end

  def update_social_media
    update_store_attributes("social_media")
  end

  def analytics 
  end

  def update_analytics
    update_store_attributes("analytics")
  end

  def details
  end

  def update_details
    update_store_attributes("details")
    @current_store.update_setup_step("details")
  end

  def domain
  end

  def update_domain
    render_not_authorized unless @current_store.can_custom_domain?
    update_store_attributes("domain")
  end

  def seo
  end

  def update_seo
    update_store_attributes("seo")
  end

  def design
  end 

  def update_design
    if design_params.nil?
      flash[:error] = 'لم يتم تحديد مرفقات بعد.'
      redirect_back(fallback_location: dashboard_dashboard_path)
    else
      update_store_attributes("design")
      @current_store.update_setup_step("design")
    end
  end

  def configuration
  end

  def update_configuration
    update_store_attributes("configuration")
    @current_store.update_setup_step("configuration")
  end

  def subscription
  end

  def upgrade_subscription
    MarketPlace::Dashboard::Subscribe.upgrade(@options, subscription_params)
    @current_plan = @current_store.plan
  rescue MarketPlace::Errors::InvalidPaymentMethod
    @error = 'عذرا! الكوبون مستخدم او غير صالح.'
  rescue MarketPlace::Errors::InvalidCoupon
    @error = 'عذرا! الكوبون مستخدم او غير صالح.'
  rescue MarketPlace::Errors::EmptyCoupon
    @error = 'فارغ، يرجى ملء الحقل'
  end

  def dashboard
  end

  private 

  # Never trust parameters from the scary internet, only allow the white list through.
  def new_store_params
    params.require(:store).permit(:ident, users_attributes: [:email, :password, :password_confirmation, :fullname])
  end

  def social_media_params
    params.require(:store).permit(:ident)
  end

  def analytics_params
    params.require(:store).permit(:google_analytics_key)
  end

  def details_params
    params.require(:store).permit(:ident, :slogan, :description, :email, :mobile, :address)
  end

  def seo_params
    params.require(:store).permit(:meta_title, :meta_description)
  end

  def domain_params
    params.require(:store).permit(:domain, :use_domain)
  end

  def design_params
    params.require(:store).permit(:favicon, :logo, :cover_photo) rescue nil
  end

  def social_media_params
    params.require(:store).permit(:facebook, :twitter, :instagram)
  end

  def configuration_params
    params[:store].delete(:enable_google_maps) unless @current_store.can_use_google_map?
    params.require(:store).permit(:currency, :min_order_total, :enable_reviews, 
      :max_pending_order, :enable_google_maps, :delivery_start_at, 
      :delivery_end_at, :delivery_amount, :free_delivery_with_amount, :google_maps_key)
  end

  def subscription_params
    params.require(:store).permit(:coupon, :plan_duration, :plan_type, :payment_method)
  end

  def update_store_attributes(partial)
    if @current_store.update(send("#{partial}_params"))
      flash[:notice] = 'تم التحديث بنجاح'
      redirect_back(fallback_location: dashboard_dashboard_path)
    else
      render partial.to_sym
    end
  end

  def set_variable
    @setup_store_tab = true
  end

end