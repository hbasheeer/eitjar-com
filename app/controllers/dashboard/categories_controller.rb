class Dashboard::CategoriesController < StoreBaseController
  before_action :authenticate_user!, :check_authorization
  before_action :set_category, except: [:create, :new, :index]
  respond_to :html

  def index
    @pagy, @categories = pagy(MarketPlace::Dashboard::Category.index(@options)[:categories])
  end

  def new
    @category = @current_store.categories.new
  end

  def edit
  end

  def create
    @category = @current_store.categories.new(category_params)
    MarketPlace::Dashboard::Category.create(@category, @options, flash)
    redirect_to dashboard_categories_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :new
  end

  def update
    MarketPlace::Dashboard::Category.update(@category, category_params, @options, flash)
    redirect_to dashboard_categories_url
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def hide
    @category.update(hidden: true)
  end

  def unhide
    @category.update(hidden: false)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = @current_store.categories.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:id, :name ,:slug, :image)
  end
end