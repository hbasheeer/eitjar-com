class Dashboard::PagesController < StoreBaseController
  before_action :authenticate_user!,
                :check_authorization,
                :set_page, only: [:edit, :update, :unpublish, :publish]

  def index
    @pages = @current_store.pages.order("created_at desc")
  end

  def new
    @page = @current_store.pages.new
  end

  def edit
    
  end

  def create
    @page = @current_store.pages.new(page_params)
    MarketPlace::Dashboard::Page.create(@page, @options)
    redirect_to dashboard_pages_url
    flash[:notice] = 'تم إضافة الصفحة بنجاح.'
  rescue MarketPlace::Errors::UnprocessableEntity
    render :new
  end

  def update
    MarketPlace::Dashboard::Page.update(@page, page_params, @options)
    redirect_to dashboard_pages_url
    flash[:notice] = 'تم تعديل الصفحة بنجاح'
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def publish
    @page.update(published: true)
  end

  def unpublish
    @page.update(published: false)
  end

  private

  def set_page
    @page = @current_store.pages.find(params[:id])
  end

  def page_params
    params.require(:page).permit(:title, :slug, :meta_description, :meta_keywords, :content, :published)
  end

end
