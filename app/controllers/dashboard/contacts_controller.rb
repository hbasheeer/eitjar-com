class Dashboard::ContactsController < ApplicationController
  
  def index
    @pagy, @contacts = pagy(@current_store.contacts.order(created_at: :desc))
  end

  def show
    @contact = @current_store.contacts.find(params[:id])
  end
end