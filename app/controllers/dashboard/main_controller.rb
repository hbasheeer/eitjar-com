class Dashboard::MainController < StoreBaseController
  before_action :authenticate_user!, :check_authorization

  def index
    @report_hash = MarketPlace::Dashboard::Main.index(@options)[:report_hash]
  end

  def report
  	period = params[:period] || "all"
  	@report_hash = MarketPlace::Dashboard::Main.report(period, @options)[:report_hash]
  	respond_to :js
  end

end