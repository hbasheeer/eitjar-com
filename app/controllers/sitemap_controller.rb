class SitemapController < StoreBaseController
  respond_to :xml
  def index
    @products = Product.active.order("created_at DESC")
    @pages = Page.where(:published => true).order("created_at DESC")
  end
end
