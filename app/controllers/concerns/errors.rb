module Errors
  extend ActiveSupport::Concern

  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class InvalidToken < StandardError; end
  class ExpiredSignature < StandardError; end
  class DecodeError < StandardError; end

  included do
    unless Rails.application.config.consider_all_requests_local
      rescue_from Exception, with: :render_500_error
      rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
      rescue_from ActionController::RoutingError, with: :render_not_found
      rescue_from Pundit::NotAuthorizedError, with: :render_not_authorized

      rescue_from Errors::AuthenticationError, with: :unauthorized_request
      rescue_from Errors::MissingToken, with: :invalid_token
      rescue_from Errors::InvalidToken, with: :invalid_token
      rescue_from Errors::ExpiredSignature, with: :invalid_token
      rescue_from Errors::DecodeError, with: :invalid_token
    end
  end

  # render 500 error
  def render_500_error(failsafe_error)
    Raven.capture_exception(failsafe_error)
    logger.error "Error during failsafe response: #{failsafe_error}\n  #{failsafe_error.backtrace * "\n  "}"
    respond_to do |f|
      f.html { render template: 'errors/500',layout: layout_by_host, status: 500 }
      f.json { render json: { error: 'error' }, status: 500 }
      f.all { render plain: 'error', status: 500 }
    end
  end

  # render 404 error
  def render_not_found
    respond_to do |f|
      f.html { render template: 'errors/404', layout: layout_by_host, status: 404 }
      f.json { render json: { error: 'not found' }, status: 404}
      f.all { render plain: 'not found', status: 404 }
    end
  end

  def render_not_authorized
    respond_to do |f|
      f.html { render template: 'errors/401',layout: layout_by_host, status: 401 }
      f.json { render json: { error: 'not authorized' }, status: 401}
      f.all { render plain: 'not authorized', status: 401 }
    end
  end

  def not_found
    fail ActionController::RoutingError.new('Not Found')
  end

  # render 403 error
  def render_unavailable
    respond_to do |f|
      f.html { render template: 'errors/403', layout: layout_by_host, status: 403 }
      f.json { render json: { error: 'forbidden' }, status: 403 }
    end
  end

  def invalid_token(e)
    render json: { message: e.message }, status: :invalid_token
  end

  def render_error(msg, errors = nil, status = :unprocessable_entity)
    json_body = { error: I18n.t(msg) }
    json_body[:errors] = errors if errors
    render json: json_body, status: status, content_type: 'application/json'
  end

  # render 503 error
  def render_maintenance
    render json: { message: 'قيد الصيانة' }, status: 503
  end
end
