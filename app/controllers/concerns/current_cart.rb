module CurrentCart
  extend ActiveSupport::Concern

  private

  def set_cart
    return unless @current_store.present?
    if current_user && current_user.cart
      @cart = current_user.cart
    elsif cookies[:cart_id].present?
      @cart = Cart.where(unique_id: cookies[:cart_id], store_id: request.env[:store_id]).first
      unless @cart
        @cart = Cart.create!(user_id: current_user.try(:id), store_id: request.env[:store_id])
        cookies.delete :cart_id
        cookies[:cart_id] = { value: @cart.unique_id, expires: 1.year.from_now, domain: @current_store.try(:full_domain) }
      end
    else
      @cart = Cart.create!(user_id: current_user.try(:id), store_id: request.env[:store_id])
      cookies[:cart_id] = { value: @cart.unique_id, expires: 1.year.from_now, domain: @current_store.try(:full_domain) }
    end
    @cart.update_attribute(:user_id, current_user.id) if current_user && @cart.user_id.nil?
    @cart
  end

  def set_cart_from_api
    return unless @current_user
    @cart = @current_user.cart ? @current_user.cart : @cart = Cart.create!(user_id: @current_user.try(:id))
  end
end

