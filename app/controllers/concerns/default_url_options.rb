module DefaultUrlOptions
  # Adds locale to all links
  def default_url_options
    { :locale => I18n.locale }
  end
end