module SharedMethods
  extend ActiveSupport::Concern

  protected

  def prepare_options
    @options = {current_user: current_user, current_store: @current_store, params: params, request: request}
  end

  def current_store
    @current_store = request.env[:current_store]
    request.env[:store_id] = @current_store.try(:id) || nil
  end

  def current_plan
    @current_plan = request.env[:current_plan]
  end

  def ensure_access_to_store_by_user
    return unless current_user && @current_store
    if current_user.store_id != request.env[:store_id]
      sign_out current_user
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:fullname, :terms, store_attributes: [:ident]])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:fullname, :mobile, :address])
  end

end