module Authenticatable
  def authenticate_user_from_token!
    if valid_jwt_token?
      @current_user = user
    else
      @current_user = nil
    end
  end

  def jwt_payload
    @jwt_payload ||= begin
      auth_header = request.headers['Authorization']
      token = auth_header.split(' ').last
      JsonWebToken.decode(token)
    rescue
      nil
    end
  end

  def user
    @user ||= begin
      User.where(id: jwt_payload[0]['user']['id']).first
    rescue
      nil
    end
  end

  def valid_jwt_token?
    id = jwt_payload[0]['user']['id']
    id.present? 
  rescue
    false
  end
end