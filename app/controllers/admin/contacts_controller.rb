class Admin::ContactsController < Admin::AdminController

  def index
    @contacts = Contact.all.order("created_at desc").page params[:page]
  end

end