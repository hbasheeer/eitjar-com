class Admin::PagesController < Admin::AdminController

  #before_action :authenticate_user!
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  def index
    #authorize Page
    @pages = Page.all.order("created_at desc").page params[:page]
  end


  def show
    #authorize @page
  end


  def new
    @page = Page.new
    #authorize @page
  end


  def edit
    #authorize @page
  end


  def create
    @page = Page.new(page_params)
    #authorize  @page

    respond_to do |format|
      if @page.save
        format.html { redirect_to admin_pages_url, notice: 'تم إضافة الصفحة بنجاح.' }
        format.json { render action: 'show', status: :created, location: @page }
      else
        format.html { render action: 'new' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    #authorize @page
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to admin_pages_url, notice: 'تم التعديل بنجاح.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to admin_pages_url, notice: 'تم حذف الصفحة بنجاح.' }
      format.json { head :no_content }
    end
  end

  private
    def set_page
      @page = Page.find(params[:id])
    end

    def page_params
      params.require(:page).permit(:title, :slug, :meta_description, :meta_keywords, :content, :published)
    end
end