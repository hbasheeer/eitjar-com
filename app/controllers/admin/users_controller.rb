class Admin::UsersController < Admin::AdminController
  before_action :set_user, except: [:index]

  # autocomplete :user, :name

  # GET /admin/users
  # GET /admin/users.json
  def index
    search_users
  end

  # GET /admin/users/1
  # GET /admin/users/1.json
  def show
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admin_user_url(@user.id), notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def ban
    @user.lock_access!(send_instructions: false)
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def unban
    @user.unlock_access!
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'User was successfully unlocked.' }
      format.json { head :no_content }
    end
  end


  def reset_password
    @user.send_reset_password_instructions
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'Rest pass was successfully done.' }
      format.json { head :no_content }
    end
  end

  def reset_mobile_attempts
    @user.reset_mobile_attempts
    log_action('ADMIN_RESET_USER_MOBILE_ATTEMPTS', {}, @user, current_user)
    respond_to do |format|
      format.html { redirect_to admin_user_url(@user), notice: 'Rest mobile attempts was successfully done.' }
      format.json { head :no_content }
    end
  end

  def reset_mobile
    old_mobile = @user.mobile
    old_mobile_iso2 = @user.mobile_iso2
    @user.reset_mobile
    log_action('ADMIN_RESET_USER_MOBILE', {mobile: old_mobile, mobile_iso2: old_mobile_iso2}, @user, current_user)
    respond_to do |format|
      format.html { redirect_to admin_user_url(@user), notice: 'Rest mobile was successfully done.' }
      format.json { head :no_content }
    end
  end

  def confirm_mobile
    @user.confirm_mobile!
    log_action('ADMIN_CONFIRM_USER_MOBILE', {}, @user, current_user)
    respond_to do |format|
      format.html { redirect_to admin_user_url(@user), notice: 'Rest mobile was successfully done.' }
      format.json { head :no_content }
    end
  end

  def change_password
    @user.update_attributes!(password: params[:password])
    @user.force_logout
    log_action('ADMIN_CHANGE_USER_PASSWORD', {}, @user, current_user)
    respond_to do |format|
      format.html { redirect_to admin_user_url(@user), notice: 'Rest pass was successfully done.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find params[:id]
  end

  def search_users
    @q = params[:q]
    @users = User.order(created_at: :desc).page params[:page]
    search_where_clause = 'email LIKE ? OR fullname LIKE ?'
    show_only_banned_where_clause = 'locked_at IS NOT NULL'
    if params[:show_only_banned].nil?
      unless @q.blank?
        @users = User.where(search_where_clause, "%#{@q}%", "%#{@q}%").order(created_at: :desc).page params[:page]
      end
    else
      if @q.blank?
        @users = User.where(show_only_banned_where_clause).order(created_at: :desc).page params[:page]
      else
        show_only_banned_where_clause << " AND #{search_where_clause}"
        @users = User.where(show_only_banned_where_clause, "%#{@q}%", "%#{@q}%").order(created_at: :desc).page params[:page]
      end
    end
  end

  def user_params
    params.require(:user).permit(:id, :fullname, :username, :email ,:mobile, :role, :country)
  end
end
