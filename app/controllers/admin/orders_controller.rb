class Admin::OrdersController < Admin::AdminController
  before_action :set_order, except: [:index]

  def index
     filter_orders
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to admin_orders_url, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def filter_orders
    @filter = params[:filter]
    if @filter.present?
      @orders = Order.send(@filter).order(created_at: :desc).page params[:page]
    else
      @orders = Order.order(created_at: :desc).page params[:page]
    end

  end
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find params[:id]
  end

  def order_params
    params.require(:order).permit(:id, :detail, :user_id, :order_cart_id, :status, :latitude, :longitude, :country, :mobile, :store_id)
  end
end
