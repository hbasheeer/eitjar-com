class Admin::ProductsController < Admin::AdminController
  before_action :set_product, except: [:create, :new, :index]


  def index
    filter_products
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_products_url, notice: 'Product was successfully created.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to admin_products_url, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  private


  def filter_products
    @filter = params[:filter]
    if @filter.present?
      @products = Product.send(@filter).order(created_at: :desc).page params[:page]
    else
      @products = Product.order(created_at: :desc).page params[:page]
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find params[:id]
  end

  def product_params
    params.require(:product).permit(:id, :title, :description, :category_id, :price, :status, :in_stack)
  end
end
