class Admin::ProductOffersController < Admin::AdminController
  before_action :set_offer, except: [:create, :new, :index]


  def index
    @offers = ProductOffer.all.order("created_at desc").page params[:page]
  end

  def new
    @offer = ProductOffer.new
  end

  def edit
  end

  def create
    @offer = ProductOffer.new(product_params)
    respond_to do |format|
      if @offer.save
        format.html { redirect_to admin_product_offers_url, notice: 'Offer was successfully created.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @offer.update(product_params)
        format.html { redirect_to admin_product_offers_url, notice: 'Offer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  private


  def filter_products
    @filter = params[:filter]
    if @filter.present?
      @products = Product.send(@filter).order(created_at: :desc).page params[:page]
    else
      @products = Product.order(created_at: :desc).page params[:page]
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_offer
    @offer = ProductOffer.find params[:id]
  end

  def product_params
    params.require(:product_offer).permit(:id, :product_id, :start_at ,:end_at, :price, :active)
  end
end


