class Admin::ReviewsController < Admin::AdminController
  before_action :set_review, except: [:create, :new, :index]


  def index
    @reviews = Review.order(created_at: :desc).page params[:page]
  end

  def edit
  end

  def update
    respond_to do |format|
      if @review.update(review_params)
        format.html { redirect_to admin_reviews_url, notice: 'Review was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  private


  # Use callbacks to share common setup or constraints between actions.
  def set_review
    @review = Review.find params[:id]
  end

  def review_params
    params.require(:review).permit(:id, :user_id, :product_id ,:rate, :content, :hidden)
  end
end
