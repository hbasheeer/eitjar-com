module Admin
  class AdminController < ApplicationController
    layout 'admin'
    before_action :check_user
    
    def check_user
      if !user_signed_in? || !current_user.admin?
        render_not_found
      end
    end

  end
end
