class StoreBaseController < ApplicationController
  layout "store_base"
  before_action :set_visual_identity
  def check_authorization
    render_not_found if @current_store.owner != current_user
  end

  def set_visual_identity
    @current_store_logo = @current_store.logo.attached? ? url_for(@current_store.logo) :  nil
    @current_store_cover = if @current_store.cover_photo.attached? 
        url_for(@current_store.cover_photo)
      else
        ActionController::Base.helpers.image_path('default.jpg')
      end
    @current_store_favicon = @current_store.logo.attached? ? url_for(@current_store.favicon) :  nil
  end

end