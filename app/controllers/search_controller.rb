class SearchController < StoreBaseController
  before_action :set_categories
  def search
    if params[:q]
       @products = Product.active.where('ar_name LIKE :q OR ku_name LIKE :q OR ar_description LIKE :q OR ku_description LIKE :q', q: "%#{params[:q]}%").page(params[:page])
    else
      @products = Product.active.page(params[:page])
    end
  end
end
