class StoresController < StoreBaseController
  layout "store_base"

  def show
    @products = @current_store.products.featured
  end

end