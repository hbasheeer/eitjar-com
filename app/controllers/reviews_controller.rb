class ReviewsController < StoreBaseController
  before_action :authenticate_user!, :set_product

  def create
    if @product && !current_user.reviews.map(&:product_id).include?(@product.id)
      @review = @current_store.reviews.new(review_params)
      @review.user = current_user
      @review.save
    else
      # raise error here
    end
  end

  private 

  # Never trust parameters from the scary internet, only allow the white list through.
  def review_params
    params.require(:review).permit(:user_id, :rate, :content, :product_id)
  end

  def set_product
    @product = @current_store.products.find(review_params[:product_id])
  end

end