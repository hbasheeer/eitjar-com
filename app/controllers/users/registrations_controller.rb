class Users::RegistrationsController < Devise::RegistrationsController
  before_action :check_store_plan

  def new
    build_resource({})
    self.resource.store = @current_store || Store.new
    respond_with self.resource
  end

  def create
    build_resource(sign_up_params)
    # signup from main domain must build Store with User
    if resource.try(:store).try(:new_record?)
      # set the owner of store 
      resource.store.owner = resource
    else
      resource.store = @current_store
    end
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        unless @current_store
          MarketPlace::Store.handle_new_store(resource.store)
        end
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def after_update_path_for(resource)
    edit_user_registration_path()
  end

  # override require_no_authentication to prevint redirect_to in main sign_up page if user has session
  def require_no_authentication
    return unless @current_store
    super
  end

  def check_store_plan
    render_unavailable unless @current_store.try(:can_add_user?)
  end
end