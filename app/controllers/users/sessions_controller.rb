class Users::SessionsController < Devise::SessionsController

  def new
    unless @current_store
      render_not_found
    else 
      super
    end
  end
end