class CartItemsController < StoreBaseController
  before_action :set_cart_item, only: [:show, :edit, :update, :destroy]

  def create
    @product = Product.find(params[:product_id])
    render_not_found if !@product.available?
    if params[:quantity] && params[:quantity].to_i > 1
      quantity = params[:quantity].to_i
    else
      quantity = 1
    end
    @cart_item = @cart.add_product(@product, @current_store.try(:id), quantity)
    @cart_item.save
  end

  def decrease
    @product = Product.find(params[:product_id])
    render_not_found unless @product
    @cart_item = @cart.remove_product(@product.id)
    @cart_item.save
  end

  def increase
    @product = Product.find(params[:product_id])
    render_not_found unless @product
    @cart_item = @cart.add_product(@product, @current_store.try(:id))
    @cart_item.save
  end

  def destroy
    @cart_item.destroy
  end

  private
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    def cart_item_params
      params.require(:cart_item).permit(:product_id, :cart_id, :order_id)
    end
end
