class ProductsController < StoreBaseController
  #before_action :set_categories
  before_action :set_category, only: [:index]
  before_action :set_product, only: [:show]

  def index
    @pagy, @products = pagy(MarketPlace::Product.index(@category, @options)[:products])
  end

  def show
    @similar_products = MarketPlace::Product.similar(@product, @options)[:products]
    @review = @current_store.reviews.new
  rescue MarketPlace::Errors::NotFound
    render_not_found
  end

  private
  
  def set_product
    @product = @current_store.products.find(params[:id])
  end

  def set_category
    return if params[:category_slug].nil?
    @category = @current_store.categories.find_by(slug: params[:category_slug])
    render_not_found unless @category
  end
end

