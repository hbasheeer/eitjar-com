class ApplicationController < ActionController::Base
  layout :layout_by_host

  include CurrentCart
  include SharedMethods
  include Pagy::Backend
  include Pundit
  include Errors
  #include DefaultUrlOptions

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :current_store,
                :current_plan,
                :prepare_options,
                :set_cart,
                :ensure_access_to_store_by_user
                #:redirect_to_subdomain,
                #:set_locale
 
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale || cookies[:locale] 
    @locale = I18n.locale
  end

  def layout_by_host
    request.env[:current_store] ? "store_base" : "application"
  end

  def redirect_back_or_default(default)
    redirect_to(session['user_return_to'] || request.env['HTTP_REFERER'] || default)
    session['user_return_to'] = nil
  end

  #redirects to subdomain on signup
  def redirect_to_subdomain 
    return if self.is_a?(DeviseController)
    if current_user && @current_store && request.host != MarketPlace::Utility::Url.strip_port_from_host(@current_store.domain)
      redirect_to @current_store.full_url
    end
  end

  def after_sign_in_path_for(resource_or_scope)
    resource_store = resource_or_scope.store

    if @current_store.present?
      resource_store.owner_id.eql?(@current_store.id) ?  resource_store.dashboard_url : resource_store.full_url 
    else
      stored_location_for(resource_or_scope) || signed_in_root_path(resource_or_scope)
    end
  end

end
