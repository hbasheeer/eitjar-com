class LocationsController < StoreBaseController
  before_action :authenticate_user!, only: [:new, :create, :cancel, :index]
  before_action :set_location, only: [:show, :edit, :update, :destroy, :cancel]
  respond_to :html

  def index
    @pagy, @locations = pagy(MarketPlace::Location.index(@options)[:locations])
  end

  def new
    @location = @current_store.locations.new
  end

  def create
    @location = @current_store.locations.new(location_params)
    MarketPlace::Location.create(@location, @options)[:location]
    redirect_to locations_url
    flash[:notice] = 'تم إضافة العنوان بنجاح'
  rescue MarketPlace::Errors::UnprocessableEntity
    render :new
  end

  def edit
  end

  def update
    location = MarketPlace::Location.update(@location, location_params, @options)[:location]
    redirect_to  edit_location_path(location)
    flash[:notice] = 'تم تعديل العنوان بنجاح'
  rescue MarketPlace::Errors::UnprocessableEntity
    render :edit
  end

  def destroy
    if @location.destroy
      redirect_back(fallback_location: locations_url)
      flash[:notice] = 'تم حذف العنوان بنجاح'
    end 
  end


  private
  
  def set_location
    @location = Location.where(id: params[:id], store_id: @current_store.id, user_id: current_user.id).first
  end

  def location_params
    params.require(:location).permit(:location_type, :latitude, :longitude, :google_address, :address, :fullname, :mobile)
  end
end

