class OrdersController < StoreBaseController
  before_action :authenticate_user!, only: [:new, :create, :cancel, :index]
  before_action :set_order, only: [:show, :edit, :update, :destroy, :cancel]
  before_action :check_cart, only: [:new, :create]
  before_action :can_user_add_order?, only: [:new, :create]
  respond_to :html

  def index
    @pagy, @orders = pagy(MarketPlace::Order.index(params[:status], @options)[:orders])
  end

  def show
    render_not_found if !current_user || @order.user != current_user
  end

  def cancel
    @order.cancel_order
    redirect_to @order
  end

  def new
    @order = @current_store.orders.new
  end

  def create
    @order = @current_store.orders.new(order_params)
    respond_with MarketPlace::Order.create(@order, @options)[:order]
  rescue MarketPlace::Errors::UnprocessableEntity
    puts @order.errors
    render :new
  end

  private
  
  def set_order
    @order = Order.where(id: params[:id], store_id: @current_store.id, user_id: current_user.id).first
  end

  def check_cart
    if @cart.items.empty? || @cart.price < @current_store.min_order_total
      redirect_to cart_url
    end
  end

  def can_user_add_order?
    if !current_user.can_add_order?
      redirect_to cart_path
      flash[:alert] = "عذرا، قد تجاوزت الحد المسموح به للطلبات الجديدة"
    end
  end

  def order_params
    params.require(:order).permit(:detail, :location_id)
  end
end
