class ContactsController < ApplicationController
  
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params.merge!(store_id: request.env[:store_id]))
    MarketPlace::Contact.create(@contact, @options, flash)
    redirect_to contacts_path 
  rescue MarketPlace::Errors::UnprocessableEntity
    render :new
  end

private

  def contact_params
    params.require(:contact).permit(:name, :subject, :mobile, :email, :message)
  end

end