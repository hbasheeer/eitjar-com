class Notifier < ApplicationMailer

  def new_contact_message(message)
    @message = message
    mail(
      to: Settings.mailer.to, 
      subject: "رسالة من #{message.name} - Xera Bazar", 
      from: "#{message.name} <#{message.email}>",
      reply_to: @message.email,
    )
  end

  def new_order_message(order)
    @order_store = order.store
    mail( 
      to: order.user.email, 
      from: @order_store.from_mail,
      reply_to: @order_store.email,
      subject: "تأكيد الطلب على #{@order_store.slogan}"
    )
  end

  def complete_order_message(order)
    @order_store = order.store
    mail( 
      to: order.user.email, 
      from: @order_store.from_mail,
      reply_to: @order_store.email,
      subject: "اكتمال الطلب ##{order.id} على #{@order_store.slogan}"
    )
  end

  def executing_order_message(order)
    @order_store = order.store
    mail( 
      to: order.user.email, 
      from: @order_store.from_mail,
      reply_to: @order_store.email,
      subject: "اكتمال الطلب ##{order.id} على #{@order_store.slogan}"
    )
  end

  def cancelled_order_message(order)
    @order_store = order.store
    mail( 
      to: order.user.email, 
      from: @order_store.from_mail,
      reply_to: @order_store.email,
      subject: "إلغاء الطلب ##{order.id} على #{@order_store.slogan}"
    )
  end
end
