class UserNotifier < Devise::Mailer

  # for devise templates
  def headers_for(action, opts)
    super.merge!({
         template_path: "/users/mailers/#{I18n.locale}",
         from: @resource.store.from_mail,
         reply_to: @resource.store.reply_to_mail


      })
  end

end