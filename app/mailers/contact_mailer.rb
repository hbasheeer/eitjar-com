class ContactMailer < ApplicationMailer

  def new_contact(contact)
  	@contact = contact
    mail(to: Settings.site_email, subject: "#{contact.subject}", from: "#{contact.name} <#{contact.email}>")
  end
end
