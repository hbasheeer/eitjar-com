class ApplicationMailer < ActionMailer::Base
  default template_path: 'mailer', 'Message-ID' => ->() { "<#{SecureRandom.uuid}@#{Settings.mailgun.domain}>" }

  layout 'mailer'
end

