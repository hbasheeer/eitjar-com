
ActionMailer::Base.delivery_method = :mailgun
ActionMailer::Base.default_url_options = { host: Rails.application.credentials[Rails.env.to_sym][:mailer][:host] }
ActionMailer::Base.mailgun_settings = {
api_key: Rails.application.credentials[Rails.env.to_sym][:mailgun][:api_key],
domain: Rails.application.credentials[Rails.env.to_sym][:mailgun][:domain]
}
