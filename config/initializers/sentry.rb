Raven.configure do |config|
  config.dsn = Rails.application.credentials[Rails.env.to_sym][:sentry][:key]
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.environments = %w(staging production)
  config.excluded_exceptions = [
    'AbstractController::ActionNotFound',
    'ActionController::RoutingError',
    'ActiveRecord::RecordNotFound',
    'ActionController::BadRequest',
    'ActionController::InvalidAuthenticityToken'
  ]
end
