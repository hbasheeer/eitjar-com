require 'carrierwave/orm/activerecord'
require 'fog/aws'

if Rails.env.test? || Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
  end
else
  CarrierWave.configure do |config|
    config.fog_provider = 'fog/aws'                        
    config.fog_credentials = {
      provider:              'AWS',                       
      aws_access_key_id:     Rails.application.credentials[Rails.env.to_sym][:s3][:access_key_id],
      aws_secret_access_key: Rails.application.credentials[Rails.env.to_sym][:s3][:secret_access_key],
      region:                Rails.application.credentials[Rails.env.to_sym][:s3][:region],
    }
    config.fog_directory  = Rails.application.credentials[Rails.env.to_sym][:s3][:bucket]
    config.fog_public     = true
    config.fog_attributes = { cache_control: "public, max-age=#{365.day.to_i}" } 
    #config.asset_host = Rails.application.credentials[Rails.env.to_sym][:asset_host]
  end
end
