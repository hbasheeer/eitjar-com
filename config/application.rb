require_relative 'boot'

require 'rails/all'
require File.expand_path('../../lib/store_middleware', __FILE__)

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MarketPlace
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    config.autoloader = :classic

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    #config.active_job.queue_adapter = :sidekiq
    # config.i18n.default_locale = :ar
    #config.exceptions_app = self.routes
    config.eager_load_paths << Rails.root.join('lib')

    # Resolve current marketplace and append it to env
    config.middleware.use ::MarketplaceLookup
    config.time_zone = "Baghdad"
  end
end
