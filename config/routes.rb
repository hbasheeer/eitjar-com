Rails.application.routes.draw do
  # move omniauth_callbacks devise module out the scope ':locale' when use it
  # devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'omniauth_callbacks'}

  # use it when you add a new language to site
  # scope ':locale', locale: /#{I18n.available_locales.join("|")}/ do

  constraints(RootStoreRoutes) do
    get '/', to: 'stores#show'
  end
  
  constraints(!RootStoreRoutes) do
    get '/', to: 'home#landing_page', as: :root
  end

  devise_for :users,
    controllers: {
      registrations: 'users/registrations',
      sessions: 'users/sessions',
      omniauth_callbacks: 'users/omniauth_callbacks'
    },
    :path => "/",
    :path_names => { :sign_in =>       'login',
                     :sign_out =>      'logout',
                     :sign_up =>       '',
                     :registration =>  'register',
                     :edit =>          'profile',
                     :cancel =>        'cancel',
                     :confirmation =>  'verification'
                    }

  get "/profile", to: "users/registrations#edit", as: :profile
  put "/profile", to: "users/registrations#update"
  
  namespace :dashboard do
    root 'main#index', as: :dashboard
    get '/ajax/report', to: "main#report"

    resources :products, except: [:destroy]
    resources :pages, except: [:show, :destroy] do
      member do 
        put 'publish'
        put 'unpublish'
      end
    end

    resources :categories, except: [:show, :destroy] do
      member do 
        put 'hide'
        put 'unhide'
      end
    end

    resources :reviews, except: [:show, :destroy, :create] do
      member do 
        put 'hide'
        put 'unhide'
      end
    end

    resources :users, except: [:show, :destroy, :create] do 
      member do 
        put 'ban'
        put 'unban'
      end
    end
    resources :orders, except: [:destroy, :create]

    get :me, to: "users#me"
    put :me, to: "users#update_me" 

    scope :setup do 
      get :social_media, to: 'setup_stores#social_media'
      get :analytics,  to: 'setup_stores#analytics'
      get :details,  to: 'setup_stores#details'
      get :domain,  to: 'setup_stores#domain'
      get :seo,  to: 'setup_stores#seo'
      get :design, to: 'setup_stores#design'
      get :configuration, to: 'setup_stores#configuration'
      get :subscription, to: 'setup_stores#subscription'
      put :social_media, to: 'setup_stores#update_social_media'
      put :analytics, to: 'setup_stores#update_analytics'
      put :details, to: 'setup_stores#update_details'
      put :domain,  to: 'setup_stores#update_domain'
      put :seo,  to: 'setup_stores#update_seo'
      put :design, to: 'setup_stores#update_design'
      put :configuration, to: 'setup_stores#update_configuration'
      post :subscription, to: 'setup_stores#upgrade_subscription'
    end
  end

  # Sitemap
  resources :sitemap, only: [:index]
  
  resources :reviews
  resources :locations, except: [:show]
  resources :cart_items do
     member do 
      post 'decrease'
      post 'increase'
     end
  end
  
  resources :orders do 
    member do 
      put 'cancel'
    end
  end

  # Cart
  get 'cart' => "cart#index"

  # Search
  get '/search' => "search#search"

  # Contact Us 
  post '/p/contact-us', to: "contacts#create", as: :contact_us
  get '/p/contact-us', to: "contacts#new", as: :contacts

  # Dynamic pages
  get "/p/:slug", :to => "page#show"
  
  # Products
  get '/products/:category_slug/:id' => 'products#show', as: :product
  get '/products/:category_slug/' => 'products#index', as: :product_category
  get '/products' => 'products#index', as: :products

  # Favorites route
  post "products/:product_id/favorite" => "favorites#create", :as => :favorite
  delete "favorites/:product_id/destroy" => "favorites#destroy", :as => :destroy_favorite
  get '/favorites',to: "favorites#index",as: :favorites
  
  # admin
  namespace :admin do
    root 'dashboard#index'
    resources :users, except: [:destroy] do
      member do
        get :orders
        put :ban
        put :unban
      end
    end
    resources :categories, except: [:destroy]
    resources :products, except: [:destroy]
    resources :product_offers, except: [:destroy]
    resources :orders, only: [:index, :edit, :update, :show]
    resources :contacts, only: [:index]
    resources :pages, except: [:show]
    resources :reviews, only: [:index, :edit, :update]
  end

  # Errors 
  get "/404", :to => "errors#not_found"
  get "/403", :to => "errors#unavailable"
  get "/500", :to => "errors#internal_server_error"
  

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/api/graphql"
  end

  # API 
  namespace :api do
    post "/graphql", to: "graphql#execute"
    post "/Settings.telegram.token" => 'telegram_bot#webhook'
  end

  # All paths with no locale specified will be redirected to the same paths with default locale
  #root to: redirect("/#{I18n.default_locale}", status: 302), as: :redirected_root
  #get "/*path", to: redirect("/#{I18n.default_locale}/%{path}", status: 302), constraints: {path: /(?!(#{I18n.available_locales.join("|")})\/).*/}, format: false

  #get '*path', :to => 'errors#not_found', via: :all
end
